// import * as functions from 'firebase-functions';
// import * as http from 'http';
// // import { Drawing } from '../../src/models/drawing.model';

// const path = require('path');

// const { GoogleAuth } = require('google-auth-library');
// const admin = require('firebase-admin');
// admin.initializeApp();
// const db = admin.firestore();

// exports.pdf_staging_trigger = functions.storage.bucket("pdf_staging").object().onFinalize(async event => {
//   console.log('1', 'pdf_staging_trigger');
//   const filePath = event.name;
//   const fileName = path.basename(filePath);

//   console.log('2', 'fileName = ' + fileName);
//   if (fileName.endsWith('.png')) {
//     console.log('PNG!', 'MyTrigger 22');
//     // return processPng(filePath);
//     return null;
//   };
//   if (fileName.endsWith('.pdf')) {
//     console.log('PDF!', 'MyTrigger');
//     return processPdf(filePath);
//   } else {
//     return false;
//   }
//   // const newName = path.basename(filePath, '.pdf') + '.png';
//   // const tempNewPath = path.join(os.tmpdir(), newName);

// });

// function processPdf(filePath : any) {
//   console.log('processPdf!', 'processPdf!!');
//   const fileName = path.basename(filePath);
//   let data = '';
//   return new Promise((resolve, reject) => {
//     const hostname = "52.191.118.177:8080/FieldReport/MyServlet?var=" + fileName;
//     const request = http.get(`http://${hostname}`, (res) => {
//       res.on('data', (d) => {
//         data += d;
//       });
//       res.on('end', resolve);
//     });
//     request.on('error', reject);
//   }).then(() => console.log("Did it! data = " + data));
// };

// exports.fieldreport_trigger = functions.storage.bucket("fieldreportapp").object().onFinalize(async event => {
//   console.log('fieldreportapp_trigger');
//   const filePath = event.name;
//   const fileName = path.basename(filePath);
//   if (fileName.endsWith('.pdf')) {
//     console.log('PDF!', 'MyTrigger');
//     await learnIt(filePath).catch(console.error);
//     return true;
//   } else {
//     return false;
//   }

// });

// async function learnIt(filePath: any) {
//   // var projId = 'automlproj-269213';
//   // var modelId = 'TEN7501426876594782208';
//   const modelName = 'projects/automlproj-269213/locations/us-central1/models/TEN7501426876594782208';
//   const url = `https://automl.googleapis.com/v1/${modelName}:predict`

//   const bucketName = 'fieldreportapp';

//   const auth = new GoogleAuth({
//     scopes: 'https://www.googleapis.com/auth/cloud-platform'
//   });

//   const client = await auth.getClient();

//   const request = JSON.stringify({
//     payload: {
//       document: {
//         input_config: {
//           gcs_source: {
//             input_uris: "gs://" + bucketName + "/" + filePath
//           }
//         }
//       }
//     }
//   });
//   const res = await client.request({ url, method: "POST", body: request });

//   var trade_sheetno = 'trade_sheetno';
//   var sheet_name = 'sheet_name';
//   console.log(JSON.stringify(res.data));
//   if (res.data.payload) {
//     console.log("res.data.payload.length = " + res.data.payload.length);
//     if (res.data.payload[0] && res.data.payload[0].textExtraction) {
//       sheet_name = JSON.stringify(res.data.payload[0].textExtraction);
//     }
//     if (res.data.payload[1] && res.data.payload[1].textExtraction) {
//       trade_sheetno = JSON.stringify(res.data.payload[1].textExtraction);
//     }
//   }

//   console.log("annotationSpecId 0_ = " + trade_sheetno);
//   console.log("annotationSpecId 1_ = " + sheet_name);
//   // console.log("content 0 = " + res.data.payload[0].textExtraction.content);
//   // console.log("content 1 = " + res.data.payload[1].textExtraction.content);

//   console.log("about to save");

//   // var drawing = new Drawing();
//   // // drawing.id = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
//   // drawing.name = filePath;
//   // drawing.trade = trade_sheetno;
//   // drawing.description = sheet_name;

//   console.log("drawing = ");

//   db.collection('drawings').add(
//     {
//       'name' : filePath,
//       'trade' : trade_sheetno,
//       'description' : sheet_name
//     })
//     .then((ref: any) => {
//       console.log("Document successfully written! ID : " + ref.id)
//     })
//     .catch((error: any) => {
//       console.error("Error writing document: " + error);
//     });
  
//   console.log("completed update!");

// }