"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const http = require("http");
const admin = require('firebase-admin');
admin.initializeApp();
const gcs = require('@google-cloud/storage');
const path = require('path');
const os = require('os');
const https = require('https');
const { google } = require('googleapis');
const ml = google.ml('v1');
const { AutoMlClient } = require('@google-cloud/automl').v1;
exports.myTrigger = functions.storage.bucket("fieldreportapp").object().onFinalize((event) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('1', 'MyTrigger');
    const filePath = event.name;
    const fileName = path.basename(filePath);
    console.log('2', 'fileName = ' + fileName);
    if (fileName.endsWith('.png')) {
        console.log('PNG!', 'MyTrigger');
        return processPng(filePath);
        '';
    }
    ;
    if (fileName.endsWith('.pdf')) {
        console.log('PDF!', 'MyTrigger');
        yield learnIt(filePath).then().catch();
        return processPdf(filePath);
    }
    else {
        return false;
    }
}));
function processPdf(filePath) {
    console.log('processPdf!', 'processPdf!!');
    const fileName = path.basename(filePath);
    let data = '';
    return new Promise((resolve, reject) => {
        const hostname = "52.191.118.177:8080/FieldReport/MyServlet?var=" + fileName;
        const request = http.get(`http://${hostname}`, (res) => {
            res.on('data', (d) => {
                data += d;
            });
            res.on('end', resolve);
        });
        request.on('error', reject);
    }).then(() => console.log("Did it! data = " + data));
}
;
function learnIt(filePath) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log('learnIt!', 'LearnIt');
        var client_email = 'my-service@automlproj-269213.iam.gserviceaccount.com';
        var private_key = 'some_private_key';
        var scope = 'https://www.googleapis.com/auth/cloud-platform';
        var bucketName = "fieldreportapp";
        const { JWT } = require('google-auth-library');
        const keys = {
            "type": "service_account",
            "project_id": "automlproj-269213",
            "private_key_id": "some_private_key_id",
            "private_key": "some_private_key",
            "client_email": "my-service-account@automlproj-269213.iam.gserviceaccount.com",
            "client_id": "109468397208349979557",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://oauth2.googleapis.com/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/my-service-account%40automlproj-269213.iam.gserviceaccount.com"
        };
        const client = new JWT(keys.client_email, null, keys.private_key, ['https://www.googleapis.com/auth/cloud-platform']);
        const url = `https://dns.googleapis.com/dns/v1/projects/${keys.project_id}`;
        const res = yield client.request({ url });
        console.log('DNS Info:');
        console.log(res.data);
        const tokenInfo = yield client.getTokenInfo(client.credentials.access_token);
        console.log(tokenInfo);
        yield listModels(client)
            .then(() => {
            console.log("end listModels()");
        }).catch();
    });
}
function listModels(client_) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log("listModels! clientCredentials = " + client_);
        const projectId = 'automlproj-269213';
        const location = 'us-central1';
        const client = new AutoMlClient(client_);
        // Construct request
        const request = {
            parent: client.locationPath(projectId, location),
            filter: 'translation_model_metadata:*',
        };
        const [response] = yield client.listModels(request);
        console.log('List of models:');
        for (const model of response) {
            console.log(`Model name: ${model.name}`);
            console.log(`Model id: ${model.name.split('/')[model.name.split('/').length - 1]}`);
            console.log(`Model display name: ${model.displayName}`);
            console.log('Model create time');
            console.log(`\tseconds ${model.createTime.seconds}`);
            console.log(`\tnanos ${model.createTime.nanos / 1e9}`);
            console.log(`Model deployment state: ${model.deploymentState}`);
        }
    });
}
function processPng(filePath) {
    console.log('processPng!', 'processPng!!');
    return "Hello!";
}
;
exports.helloWorld = functions.https.onRequest((request, response) => {
    response.send("Hello from Firebase!");
});
//# sourceMappingURL=index copy.js.map