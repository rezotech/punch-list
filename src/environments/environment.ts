// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDzFl5IxrW2tvIRhLerouKZ9GW9R1RJXR4",
    authDomain: "punchlist-e796c.firebaseapp.com",
    databaseURL: "https://punchlist-e796c.firebaseio.com",
    projectId: "punchlist-e796c",
    storageBucket: "fieldreportapp",
    messagingSenderId: "991431887917",
    appId: "1:991431887917:web:3b45628d6dc25d51c7fca4",
    measurementId: "G-D74TC25FP4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
