import { 
  Directive,
  Input,
  Output,
  EventEmitter,
  HostBinding,
  HostListener
} from '@angular/core';
import { DrawingcanvasComponent } from '../drawings/drawingcanvas/drawingcanvas.component';
import { Dwgmark } from 'src/models/dwgmark.model';

@Directive({
  selector: '[long-press]'
})
export class LongpressDirective {
  constructor(private host:DrawingcanvasComponent) {}

  @Input() duration: number = 1000;

  @Output() onLongPress: EventEmitter<any> = new EventEmitter();
  @Output() onLongPressing: EventEmitter<any> = new EventEmitter();
  @Output() onLongPressEnd: EventEmitter<any> = new EventEmitter();

  private pressing: boolean;
  private longPressing: boolean;
  private timeout: any;
  private mouseX: number = 0;
  private mouseY: number = 0;

  @HostBinding('class.press')
  get press() { return this.pressing; }

  @HostBinding('class.longpress')
  get longPress() { return this.longPressing; }

  @HostListener('press', ['$event'])
  onMouseDown(event) {
    // don't do right/middle clicks
    console.log("Here event.which = " + event.which);
    // if(event.which !== 1) return;

    this.mouseX = event.clientX;
    this.mouseY = event.clientY;

    this.pressing = true;
    this.longPressing = false;

    this.timeout = setTimeout(() => {
      console.log("long-press!!!!!");
      this.longPressing = true;
      this.onLongPress.emit(event);
      this.loop(event);

      var canvas = document.createElement('canvas');
      var ctx = canvas.getContext('2d');

      var dwgmark = new Dwgmark();

      var image = new Image();
      console.log("drawing image"); 
      // window.alert(event.center.x + ", " + event.center.y);
      // window.alert(this.host.baseCanvasEl.nativeElement.getBoundingClientRect().left + ", " + this.host.baseCanvasEl.nativeElement.getBoundingClientRect().top);
      // var x_ = ((event.center.x - this.host.baseCanvasEl.nativeElement.getBoundingClientRect().left)/this.host.current.z)-37;
      // var y_ = ((event.center.y - this.host.baseCanvasEl.nativeElement.getBoundingClientRect().top)/this.host.current.z)-70;
      // dwgmark.x = x_;
      // dwgmark.y = y_;
      image.src = "assets/map-marker-2-64.png";
      image.onload = (() => {
        ctx.drawImage(
          image,
          0,
          0
        )
        // this.host.context.drawImage(canvas, x_, y_);
        if(!this.host.dwg.dwgmarks) {
          this.host.dwg.dwgmarks = [];
        }
        this.host.dwg.dwgmarks.push(dwgmark);
        this.host.localStorage.setItem("dwg-" + this.host.dwg.id, this.host.dwg).subscribe(() => {
          console.log("Drawing updated.")
        });
      });
    }, this.duration);
    this.loop(event);
  }

  @HostListener('mousemove', ['$event'])
  onMouseMove(event) {
    if(this.pressing && !this.longPressing) {
      const xThres = (event.clientX - this.mouseX) > 10;
      const yThres = (event.clientY - this.mouseY) > 10;
      if(xThres || yThres) {
        this.endPress();
      }
    }
  }

  loop(event) {
    if(this.longPressing) {
      this.timeout = setTimeout(() => {
        this.onLongPressing.emit(event);
        this.loop(event);
      }, 50);
    }
  }

  endPress() {
    clearTimeout(this.timeout);
    this.longPressing = false;
    this.pressing = false;
    this.onLongPressEnd.emit(true);
  }

  @HostListener('mouseup')
  onMouseUp() { this.endPress(); }

}
