import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingsviewComponent } from './drawingsview.component';

describe('DrawingsviewComponent', () => {
  let component: DrawingsviewComponent;
  let fixture: ComponentFixture<DrawingsviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawingsviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingsviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
