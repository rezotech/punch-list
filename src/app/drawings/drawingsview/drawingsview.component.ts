import { Component, OnInit, ViewChild, ElementRef, Renderer2, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ScService } from 'src/app/services/sc.service';
import { Issue } from 'src/models/issue.model';
import { Project } from 'src/models/project.model';
import { Drawing } from 'src/models/drawing.model';
import { ConsoledialogComponent } from 'src/app/dialog/consoledialog/consoledialog.component';
import { AboutdialogComponent } from 'src/app/dialog/aboutdialog/aboutdialog.component';
import { CompdialogComponent } from 'src/app/dialog/compdialog/compdialog.component';
import { Company } from 'src/models/company.model';
import { ProjdialogComponent } from 'src/app/dialog/projdialog/projdialog.component';
import { MatDialog } from '@angular/material';
import { PdfService } from 'src/app/services/pdf.service';
import { AngularFireStorage, BUCKET } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';

@Component({
  selector: 'app-drawingsview',
  templateUrl: './drawingsview.component.html',
  styleUrls: ['./drawingsview.component.css']
})
export class DrawingsviewComponent implements OnInit {
  @ViewChild("fileinput", { read: ElementRef, static: true }) fileinputEl: ElementRef;
  @ViewChild("timer", { read: ElementRef, static: true }) timerEl: ElementRef;
  public issues: Issue[] = [];
  public projects: Project[] = [];
  public drawings: Drawing[] = [];
  uploadPercent: Observable<number>;

  constructor(
    private scService: ScService,
    protected localStorage: LocalStorage,
    private router: Router,
    public dialog: MatDialog,
    private pdfService: PdfService,
    private renderer: Renderer2,
    private route: ActivatedRoute,
    private storage: AngularFireStorage,
  ) {
    this.route.params.subscribe(params => {
      if (params['id']) {
        console.log("id = " + params['id']);
        this.localStorage.getItem<Project>(params['id']).subscribe((proj_) => {
          var proj = proj_ as Project;
          this.scService.setCurrProj(proj);
          this.scService.getDrawings().length = 0;
          for (var i = 0; i < proj.drawingkeys.length; i++) {
            // window.alert("id = " + proj.drawingkeys[i]);
            this.localStorage.getItem<Drawing>("dwg-" + proj.drawingkeys[i]).subscribe((dwg_) => {
              var dwg = dwg_ as Drawing;
              this.scService.getDrawings().push(dwg);
            });
          }
          this.updateDrawings();
        });
      }
    });
    this.scService.projects_messageSource.subscribe(() => {
      this.projects = this.scService.getProjects();
      console.log("projects.length = " + this.projects.length);
      for (var proj of this.projects) {
        console.log("proj.name = " + proj.name);
      }
    });
  }

  public listSettings() {
    if (this.scService.getCurrProj() == null) {
      return;
    }
    const dialogRef = this.dialog.open(ProjdialogComponent, {
      data: { project: this.scService.getCurrProj() },
      panelClass: 'app-full-bleed-dialog',
      width: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  public companySettings() {
    if (this.scService.getCurrComp() == null) {
      this.scService.setCurrComp(new Company());
    }
    const dialogRef = this.dialog.open(CompdialogComponent, {
      data: { company: this.scService.getCurrComp() },
      panelClass: 'app-full-bleed-dialog',
      width: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  public about() {
    const dialogRef = this.dialog.open(AboutdialogComponent, {
      panelClass: 'app-full-bleed-dialog2',
      width: '400px',
      height: '80vh'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  public console() {
    const dialogRef = this.dialog.open(ConsoledialogComponent, {
      panelClass: 'app-full-bleed-dialog2',
      width: '400px',
      height: '80vh'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  public async pdfIt() {
    if (this.scService.getCurrProj() == null) {
      return;
    }
    this.setTimer(true);
    this.pdfService.pdfIt();
    return;
  }

  public setTimer(timer: boolean) {
    if (timer) {
      this.renderer.setStyle(this.timerEl.nativeElement, "display", "inline");
    } else {
      this.renderer.setStyle(this.timerEl.nativeElement, "display", "none");
    }
  }

  public addList() {
    if (this.scService.getProjectList().projkeys == null) {
      this.scService.getProjectList().projkeys = [];
    }
    var project: Project = new Project();
    project.name = "New List " + this.scService.getProjectList().projnos;
    project.key = "p" + this.scService.getProjectList().projnos;
    this.scService.getProjectList().projnos = (this.scService.getProjectList().projnos + 1);
    this.scService.getProjectList().projkeys.push(project.key);
    this.localStorage.setItem("plist", this.scService.getProjectList()).subscribe(() => {
      console.log("projkey added!");
    });
    this.localStorage.setItem(project.key, project).subscribe(() => {
      console.log("project added!");
      this.scService.getProjects().push(project);
    });
  }

  async addPdfDrawing(e: any) {
    if (!this.scService.getCurrProj()) {
      window.alert("No Current Project!");
      return;
    } else {
      // window.alert("project key = " + this.scService.getCurrProj().key);
    }
    this.scService.getAppComp().showSpinner(true);
    // window.alert("orientation = " + orientation);

    for (let i = 0; i < e.target.files.length; i++) {
      const file = e.target.files[i];
      const filePath = 'name-your-file-path-here';
      console.log("file.name = " + file.name);

      

      const ref = this.storage.ref(file.name);
      const task = ref.put(file);
      this.uploadPercent = task.percentageChanges();
      this.uploadPercent.subscribe(percent => {
        console.log("percent = " + percent);
      });
      task.snapshotChanges().pipe(
        finalize(() => {
          if (i === (e.target.files.length - 1)) {
            console.log("Uploaded!");
            this.scService.getAppComp().showSpinner(false)
          }
        })
      ).subscribe(result => {
      },
        err => {
          console.log(err);
        });
      task.catch((err) => {
        console.log(err);
      });
    }
  }

  async addDrawing(e: any) {
    if (!this.scService.getCurrProj()) {
      window.alert("No Current Project!");
      return;
    } else {
      // window.alert("project key = " + this.scService.getCurrProj().key);
    }
    this.scService.getAppComp().showSpinner(true);

    await this.getOrientation(e.target.files[0], async (orientation) => {
      // window.alert("orientation = " + orientation);
      if (orientation >= 2 && orientation <= 8) {
        var fr = new FileReader;
        fr.onload = (async () => {
          var dataUrl = fr.result.toString();
          await this.resetOrientation(dataUrl, orientation, async (resetBase64Image) => {
            this.addPhoto2a(resetBase64Image);
          });
        });
        fr.readAsDataURL(e.target.files[0]);
      } else {
        var fr = new FileReader;
        fr.onload = (() => {
          var dataUrl = fr.result.toString();
          this.addPhoto2a(dataUrl);
        });
        fr.readAsDataURL(e.target.files[0]);
      }
    });
  }

  async addPhoto2a(dataUrl) {
    var dwg = new Drawing;

    var img = new Image;
    img.onload = (async () => {

      // window.alert(img.width + "," + img.height); // image is loaded; sizes are available
      // 8.5 x 11 @ 72 dpi = 612 x 792, so make it 572 x 752
      var width = 752;
      var height = 572;
      // dwg.scale = Math.min((width / img.width), (height / img.height));

      dwg.scale = this.scService.getCurrProj().img_qual;
      dwg.width = ((img.width * dwg.scale) * dwg.scale);
      dwg.height = ((img.height * dwg.scale) * dwg.scale);
      dwg.id = new Date().getTime() + "";

      var canvas = document.createElement('canvas');
      canvas.width = img.width * dwg.scale;
      canvas.height = img.height * dwg.scale;
      canvas.getContext("2d").drawImage(img, 0, 0, canvas.width, canvas.height);

      var img2 = new Image();
      img2.onload = (async () => {

        var canvas1 = document.createElement('canvas');
        canvas1.width = img2.width * dwg.scale;
        canvas1.height = img2.height * dwg.scale;
        canvas1.getContext("2d").drawImage(img2, 0, 0, canvas1.width, canvas1.height);
        dwg.dataUrl = canvas1.toDataURL("image/png");

        this.localStorage.setItem("dwg-" + dwg.id, dwg).subscribe(() => {
          // window.alert("Saved!");
          this.drawings.push(dwg);
          if (this.scService.getCurrProj()) {
            this.scService.getCurrProj().drawingkeys.push(dwg.id);
            this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
              console.log("project updated.");
              this.scService.getAppComp().showSpinner(false);
            });
          }
          this.fileinputEl.nativeElement.value = "";
        });
      });
      img2.src = canvas.toDataURL("image/png");
    });
    img.src = dataUrl;
  }

  public currentDwg(dwg: Drawing) {
    window.alert("dwg.id = " + dwg.id);
  }

  public drawingsView() {
    this.router.navigate(['../drawings/' + this.scService.getCurrProj().key]);
  }

  public issuesView() {
    this.router.navigate(['../issues/' + this.scService.getCurrProj().key]);
  }

  updateDrawings() {
    this.drawings = this.scService.getDrawings();
    console.log("drawings.length = " + this.drawings.length);

  }

  getOrientation = (file: File, callback: Function) => {
    var reader = new FileReader();

    reader.onload = (event: ProgressEvent) => {

      if (!event.target) {
        return;
      }

      const file = event.target as FileReader;
      const view = new DataView(file.result as ArrayBuffer);

      if (view.getUint16(0, false) != 0xFFD8) {
        return callback(-2);
      }

      const length = view.byteLength
      let offset = 2;

      while (offset < length) {
        if (view.getUint16(offset + 2, false) <= 8) return callback(-1);
        let marker = view.getUint16(offset, false);
        offset += 2;

        if (marker == 0xFFE1) {
          if (view.getUint32(offset += 2, false) != 0x45786966) {
            return callback(-1);
          }

          let little = view.getUint16(offset += 6, false) == 0x4949;
          offset += view.getUint32(offset + 4, little);
          let tags = view.getUint16(offset, little);
          offset += 2;
          for (let i = 0; i < tags; i++) {
            if (view.getUint16(offset + (i * 12), little) == 0x0112) {
              return callback(view.getUint16(offset + (i * 12) + 8, little));
            }
          }
        } else if ((marker & 0xFF00) != 0xFF00) {
          break;
        }
        else {
          offset += view.getUint16(offset, false);
        }
      }
      return callback(-1);
    };
    reader.readAsArrayBuffer(file);
  }

  async resetOrientation(srcBase64, srcOrientation, callback) {
    var img = new Image();

    img.onload = (async () => {
      var width = img.width,
        height = img.height,
        canvas = document.createElement('canvas'),
        ctx = canvas.getContext("2d");

      // set proper canvas dimensions before transform & export
      if (4 < srcOrientation && srcOrientation < 9) {
        canvas.width = height;
        canvas.height = width;
      } else {
        canvas.width = width;
        canvas.height = height;
      }

      // transform context before drawing image
      switch (srcOrientation) {
        case 2: ctx.transform(-1, 0, 0, 1, width, 0); break;
        case 3: ctx.transform(-1, 0, 0, -1, width, height); break;
        case 4: ctx.transform(1, 0, 0, -1, 0, height); break;
        case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
        case 6: ctx.transform(0, 1, -1, 0, height, 0); break;
        case 7: ctx.transform(0, -1, -1, 0, height, width); break;
        case 8: ctx.transform(0, -1, 1, 0, 0, width); break;
        default: break;
      }

      // draw image
      ctx.drawImage(img, 0, 0);

      // export base64
      callback(canvas.toDataURL());
    });

    img.src = srcBase64;
  }

  uploadIt(e: any) {
    const file = e.target.files[0];
    const filePath = 'name-your-file-path-here';
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    // observe percentage changes
    var uploadPercent = task.percentageChanges();
  }

  ngOnInit() {
    this.scService.setDrawingsViewComp(this);
    this.drawings = this.scService.getDrawings();
  }

}
