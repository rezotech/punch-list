import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Drawing } from 'src/models/drawing.model';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { DwgdeleteComponent } from './dwgdelete/dwgdelete.component';
import { ScService } from 'src/app/services/sc.service';
import { Dwgmark } from 'src/models/dwgmark.model';


@Component({
  selector: 'app-drawing',
  templateUrl: './drawing.component.html',
  styleUrls: ['./drawing.component.css']
})
export class DrawingComponent implements OnInit {
  @Input() dwg: Drawing;
  @ViewChild("baseCanvas", { read: ElementRef, static: true }) public baseCanvasEl: ElementRef;
  public context: CanvasRenderingContext2D;
  public markImage: any;

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private scService: ScService
  ) {
  }

  onClick() {
    console.log("onClick() = " + this.dwg.id);
    this.scService.setCurrDwg(this.dwg);
    this.router.navigate(['dcanvas/' + this.scService.getCurrProj().key + "/" + this.dwg.id]);
  }

  delPress() {
    console.log("onPress() = " + this.dwg.id);
    const dialogRef = this.dialog.open(DwgdeleteComponent, {
      data: { dwg: this.dwg },
      panelClass: 'app-full-bleed-dialog',
      width: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  ngOnInit() {
    console.log("ngOnInit()");
    let dwgImage = new Image();
    dwgImage.src = this.dwg.dataUrl;
    dwgImage.onload = (() => {
      var width = (window.innerWidth-40)/2;
      var height = (window.innerHeight-40)/2;
      this.dwg.scale = Math.min(width / this.dwg.width, height / this.dwg.height);
      var x_ = (this.dwg.width*this.dwg.scale);
      var y_ = (this.dwg.height*this.dwg.scale);
      this.baseCanvasEl.nativeElement.width = x_; 
      this.baseCanvasEl.nativeElement.height = y_;
      this.context.drawImage(dwgImage, 0, 0, x_, y_);
      if(!this.dwg.dwgmarks) {
        return;
      }
      this.markImage = new Image();
      this.markImage.onload = (() => {
        for (var dwgmark of this.dwg.dwgmarks) {
          // console.log("dwg x y = " + dwgmark.x + ", " + dwgmark.y);
          this.createImage(dwgmark);
        };
      });
      this.markImage.src =  "assets/map-marker-2-32.png";
    })
  }

  createImage(dwgmark: Dwgmark) {   // load an image function 
    // creates a new i each time it is called
    console.log("canvas w,h=" + this.baseCanvasEl.nativeElement.width + "," + this.baseCanvasEl.nativeElement.height);
    console.log("x,y,scale=" + (dwgmark.x) + "," + (dwgmark.y) + "," + this.dwg.scale);
    // this.context.fillRect(25, 25, 100, 100);
    this.context.fillText("#"+dwgmark.issue_no, (dwgmark.x*this.baseCanvasEl.nativeElement.width)-this.context.measureText("#"+dwgmark.issue_no).width-8, (dwgmark.y*this.baseCanvasEl.nativeElement.height)-16)
    this.context.drawImage(this.markImage, (dwgmark.x*this.baseCanvasEl.nativeElement.width)-8, (dwgmark.y*this.baseCanvasEl.nativeElement.height)-16, 16, 16);
    // this.context.strokeRect(0, 0, dwgmark.x*this.baseCanvasEl.nativeElement.width, dwgmark.y*this.baseCanvasEl.nativeElement.height);

  }

  ngAfterViewInit(): void {
    console.log("ngAfterViewInit()");
    this.context = (<HTMLCanvasElement>this.baseCanvasEl.nativeElement).getContext('2d');
  }

  onLoad(event: any) {
    console.log("image loaded!");
  }

}
