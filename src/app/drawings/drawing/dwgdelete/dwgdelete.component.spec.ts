import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DwgdeleteComponent } from './dwgdelete.component';

describe('DwgdeleteComponent', () => {
  let component: DwgdeleteComponent;
  let fixture: ComponentFixture<DwgdeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DwgdeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DwgdeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
