import { Component, OnInit, Inject } from '@angular/core';
import { Drawing } from 'src/models/drawing.model';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ScService } from 'src/app/services/sc.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dwgdelete',
  templateUrl: './dwgdelete.component.html',
  styleUrls: ['./dwgdelete.component.css']
})
export class DwgdeleteComponent implements OnInit {
  dwg: Drawing = new Drawing();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<DwgdeleteComponent>,
    protected localStorage: LocalStorage,
    private scService: ScService,
    private router: Router
  ) {
    console.log("dwg id = " + data.dwg.id);
    this.dwg = data.dwg;    
   }

  ngOnInit() {
  }

  delete() {
    const proj = this.scService.getCurrProj();
    const index = proj.drawingkeys.indexOf(this.dwg.id, 0);
    if (index > -1) {
      proj.drawingkeys.splice(index, 1);
    }
    this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
      console.log("project updated!");
    }, (error) => {
      console.error("error updating project.");
    });

    this.localStorage.removeItem('dwg-' + this.dwg.id).subscribe(() => {
      console.log("drawing deleted.");
    }, (error) => {
      console.error("error deleting drawing.");
    });
    this.scService.getDrawings().length = 0;
    for (var i = 0; i < proj.drawingkeys.length; i++) {
      // window.alert("id = " + proj.drawingkeys[i]);
      this.localStorage.getItem<Drawing>("dwg-" + proj.drawingkeys[i]).subscribe((dwg_) => {
        var dwg = dwg_ as Drawing;
        dwg.scale = .06;
        this.scService.getDrawings().push(dwg);
      });
    }
    this.scService.getDrawingsViewComp().updateDrawings();
    this.dialogRef.close();
  }

}
