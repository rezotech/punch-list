import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ScService } from 'src/app/services/sc.service';
import { Drawing } from 'src/models/drawing.model';
import { Dwgmark } from 'src/models/dwgmark.model';
import { Issue } from 'src/models/issue.model';
import { DrawingpdfserviceService } from 'src/app/services/drawingpdfservice.service';
import { Project } from 'src/models/project.model';
declare const fabric: any;

@Component({
  selector: 'app-drawingcanvas',
  templateUrl: './drawingcanvas.component.html',
  styleUrls: ['./drawingcanvas.component.css']
})
export class DrawingcanvasComponent implements AfterViewInit {

  // private dwgmarkobjs: Dwgmarkobj[] = [];
  public dwg: Drawing;
  public canvas: any;
  private currdwgmark: Dwgmark = undefined;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public localStorage: LocalStorage,
    public scService: ScService,
    public drawingpdfService: DrawingpdfserviceService,
  ) {
    this.route.params.subscribe(params => {
      if (params['id2']) {
        // console.log("id = " + params['id']);
        this.localStorage.getItem<Drawing>("dwg-" + params['id2']).subscribe((dwg_) => {
          // this.dwgmarkobjs.length = 0;
          this.dwg = dwg_ as Drawing;
          let dwgImage = new Image();
          dwgImage.onload = (() => {
            this.canvas.setWidth(window.innerWidth - 20);
            this.canvas.setHeight(window.innerHeight - 20);
            // console.log("width,height=" + this.dwg.width + "," + this.dwg.height);
            var width: number = 750;
            var height: number = 500;
            this.dwg.scale = Math.min(width / dwgImage.width, height / dwgImage.height);
            var imgInstance = new fabric.Image(dwgImage, { left: 0, top: 0, angle: 0, opacity: 1 });
            // console.log("dwg.width*scale = " + this.dwg.width + ", " + scale_);
            imgInstance.scale(this.dwg.scale);
            this.canvas.setBackgroundImage(imgInstance, this.canvas.renderAll.bind(this.canvas));
            for (var dwgmark of this.dwg.dwgmarks) {
              this.createImage(dwgmark);
            };
            // var rect = new fabric.Rect({
            //   left: 0,
            //   top: 0,
            //   width: (dwgImage.width*this.dwg.scale),
            //   height: (dwgImage.height*this.dwg.scale),
            //   fill: 'transparent',
            //   stroke: 'blue',
            //   strokeWidth: 2
            // });
            // this.canvas.add(rect);
          })
          dwgImage.src = this.dwg.dataUrl;

          // var circle = new fabric.Circle({
          //   left: 0,
          //   top: 0,
          //   radius: 8,
          //   stroke: 'yellow',
          //   strokeWidth: 2,
          //   fill: ''
          // });
          // this.canvas.add(circle);

        });
      }
      if (params['id']) {
        this.localStorage.getItem<Project>(params['id']).subscribe((proj_) => {
          var proj = proj_ as Project;
          this.scService.setCurrProj(proj);
        });
      }
    });

    this.scService.project_messageSource.subscribe((proj => {
      if (proj != null) {
        this.scService.setCurrProj(proj);
      }
    }));
  }

  dropClk() {
    this.drawingpdfService.pdfIt(this.dwg);
  }

  createImage(dwgmark: Dwgmark) {   // load an image function 
    // creates a new i each time it is called   
    var image = new Image();

    var imageInstance = new fabric.Image(image, {
      // left: dwgmark.x,
      // top: dwgmark.y,
      angle: 0,
      opacity: 1,
      width: 32,
      height: 32
    });
    imageInstance.set('issue_no', dwgmark.issue_no);
    image.onload = (() => {
      imageInstance.setControlsVisibility({
        'tl': false,
        'tr': false,
        'bl': false,
        'br': false,
        'mb': false,
        'mt': false,
        'mtr': false,
        'ml': false,
        'mr': false,
      });
      var text = new fabric.Text("#" + dwgmark.issue_no, {
        fontFamily: 'Comic Sans',
        fontSize: 18,
        fontWeight: 'bold',
      });
      text.set("top", ((image.getBoundingClientRect().height / 2) - (text.width / 2) + 2));
      text.set("left", ((image.getBoundingClientRect().width / 2) - (text.height / 2) - 10));

      console.log("dwgmark.x,y="+dwgmark.x+","+dwgmark.y);

      var group = new fabric.Group([imageInstance, text], {
        left: (dwgmark.x*this.dwg.width*this.dwg.scale) - 32,
        top: (dwgmark.y*this.dwg.height*this.dwg.scale) - 38,
      });
      group.setControlsVisibility({
        'tl': false,
        'tr': false,
        'bl': false,
        'br': false,
        'mb': false,
        'mt': false,
        'mtr': false,
        'ml': false,
        'mr': false,
      });
      group.set("issue_no", dwgmark.issue_no);
      this.canvas.add(group);
      console.log("x, y = " + dwgmark.x + ", " + dwgmark.y);
     
      // var circle = new fabric.Circle({
      //   left: dwgmark.x - 32,
      //   top: dwgmark.y - 38,
      //   radius: 8,
      //   stroke: 'yellow',
      //   strokeWidth: 2,
      //   fill: ''
      // });
      // this.canvas.add(circle);
    });
    image.src = "assets/map-marker-2-32.png";

    // var rect = new fabric.Rect({
    //   left: 0,
    //   top: 0,
    //   width: (dwgmark.x*this.dwg.width*this.dwg.scale),
    //   height: (dwgmark.y*this.dwg.height*this.dwg.scale),
    //   fill: 'transparent',
    //   stroke: 'black',
    //   strokeWidth: 1
    // });
    // this.canvas.add(rect);

  }

  ngAfterViewInit(): void {
    console.log("ngOnInit()");
    this.canvas = new fabric.Canvas('canvas');
    this.canvas.selection = false;

    var pausePanning = false;
    var selected = false;
    var zoomStartScale;
    var currentY;
    var lastX;
    var lastY;
    var yChange;
    var xChange;
    var currentX;

    this.canvas.on({
      'touch:gesture': (e: any) => {
        // window.alert("Touch!");
        if (e.e.touches && e.e.touches.length == 2) {
          pausePanning = true;
          var point = new fabric.Point(e.self.x, e.self.y);
          if (e.self.state == "start") {
            // window.alert("Touch!");
            zoomStartScale = this.canvas.getZoom();
          }
          var delta = zoomStartScale * e.self.scale;
          this.canvas.zoomToPoint(point, delta);
          pausePanning = false;
        }
      },
      'object:moved': (e: any) => {
        console.log("object:moved");
        var p = this.canvas.getPointer(e.e);
        var x = (p.x/(this.dwg.width*this.dwg.scale));
        var y = (p.y/(this.dwg.height*this.dwg.scale));
        // var obj = this.canvas.getActiveObject();
        // x = obj.left + 32;
        // y = obj.top + 38;
        if (this.currdwgmark) {
          this.currdwgmark.x = x;
          this.currdwgmark.y = y;
          this.localStorage.setItem("dwg-" + this.dwg.id, this.dwg).subscribe(() => {
            console.log("Drawing updated.")
          });
        }
        console.log("x, y=" + x + ", " + y);
      },
      'selection:created': (e: any) => {
        pausePanning = true;
        console.log("selection:created");
        console.log("selection created issue = " + e.target.get('issue_no'));
        var issueno = e.target.get('issue_no');
        for (var dwgmark of this.dwg.dwgmarks) {
          if (dwgmark.issue_no === issueno) {
            this.currdwgmark = dwgmark;
            break;
          }
        };
        selected = true;
      },
      'selection:updated': (e: any) => {
        console.log("selection:updated");
        console.log("selection updated issue = " + e.target.get('issue_no'));
        var issueno = e.target.get('issue_no');
        for (var dwgmark of this.dwg.dwgmarks) {
          if (dwgmark.issue_no === issueno) {
            this.currdwgmark = dwgmark;
            break;
          }
        };
        // this.currissueno = e.target.get('issue_no');
      },
      'selection:cleared': function (e: any) {
        console.log("selected issue cleared ");
        this.currdwgmark = undefined;
        pausePanning = false;
        selected = undefined;
      },
      'touch:drag': (e: any) => {
        if (pausePanning != false) {
          // window.alert('pausePanning!');
        }

        if (pausePanning == false && undefined != e.self.x && undefined != e.self.y) {
          // window.alert("drag!");
          currentX = e.self.x;
          currentY = e.self.y;
          xChange = currentX - lastX;
          yChange = currentY - lastY;

          if ((Math.abs(currentX - lastX) <= 50) && (Math.abs(currentY - lastY) <= 50)) {
            var delta = new fabric.Point(xChange, yChange);
            this.canvas.relativePan(delta);
          }
          lastX = e.self.x;
          lastY = e.self.y;
        }
      },
      'touch:longpress': (e: any) => {
        console.log("longpress");
        if (e.e.type != 'touchstart' && e.e.type != 'mousedown') {
          return;
        }
        if (this.scService.getCurrProj() == null) {
          console.log("returning proj null!");
          return;
        }
        if (selected) {
          console.log("selected");
          return;
        }
        console.log("unselected");

        let issue: Issue = new Issue();
        issue.no = (this.scService.getCurrProj().issues.length + 1) * 1;
        issue.open_date = new Date().getTime();
        this.scService.getCurrProj().issues.push(issue);
        this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
          console.log("issue added!");
        });

        var pointer = this.canvas.getPointer(e.e);

        var w_ = pointer.x/(this.dwg.width*this.dwg.scale);
        var h_ = pointer.y/(this.dwg.height*this.dwg.scale);
        console.log("w_="+w_+",pointer.x="+pointer.x+",w*pointer="+w_*this.canvas.width);

        var dwgmark = new Dwgmark();
        dwgmark.x = w_;
        dwgmark.y = h_;
        // dwgmark.x = pointer.x;
        // dwgmark.y = pointer.y;
        dwgmark.issue_no = issue.no;
        if (!this.dwg.dwgmarks) {
          this.dwg.dwgmarks = [];
        }
        this.dwg.dwgmarks.push(dwgmark);
        this.localStorage.setItem("dwg-" + this.dwg.id, this.dwg).subscribe(() => {
          console.log("Drawing updated.")
        });
        this.createImage(dwgmark);
      },
      'mouse:wheel': (e: any) => {
        console.log("mouse wheel!");
        console.log("zoom = " + this.canvas.getZoom());
        pausePanning = true;
        var delta = .05;
        if (e.e.wheelDelta < 0) {
          if (this.canvas.getZoom() < .2) {
            return;
          }
          delta = -.05;
        } else {
          if (this.canvas.getZoom() > 5) {
            return;
          }
        }
        console.log("zoom = " + this.canvas.getZoom());
        console.log("delta = " + delta);
        delta = this.canvas.getZoom() + delta;
        var point = new fabric.Point(300, 300);
        this.canvas.zoomToPoint(point, delta);
        pausePanning = false;
      },
      'mouse:up': (e: any) => {
        var d = new Date();
        timer = d.getTime();
      },
      'mouse:down': (e: any) => {
        var d = new Date();
        if ((d.getTime() - timer) < 300) {
          console.log('double click')
          if (this.currdwgmark && pausePanning) {
            this.router.navigate(['../issue/' + this.scService.getCurrProj().key + "/" + this.currdwgmark.issue_no]);
          }
        }
      }
    });
    var timer = 0;
  }
}
