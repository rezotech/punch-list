import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawingcanvasComponent } from './drawingcanvas.component';

describe('DrawingcanvasComponent', () => {
  let component: DrawingcanvasComponent;
  let fixture: ComponentFixture<DrawingcanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawingcanvasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawingcanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
