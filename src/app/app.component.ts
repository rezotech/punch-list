import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { AngularFireAnalytics } from '@angular/fire/analytics';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { SwUpdate } from '@angular/service-worker';
import { LocalStorage } from '@ngx-pwa/local-storage';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { Company } from 'src/models/company.model';
import { User } from 'src/models/user.model';
import { Project } from '../models/project.model';
import { ProjectList } from '../models/projectlist.model';
import { AboutdialogComponent } from './dialog/aboutdialog/aboutdialog.component';
import { ScService } from './services/sc.service';
import { Account } from 'src/models/account.model';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild("namein", { read: ElementRef, static: false }) nameEl: ElementRef;
  @ViewChild("pwin", { read: ElementRef, static: false }) pwEl: ElementRef;
  @ViewChild("myspinner", { read: ElementRef, static: true }) spinnerEl: ElementRef;
  public _update: boolean = false;
  public clickedItem: string;

  constructor(
    public afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private updates: SwUpdate,
    protected localStorage: LocalStorage,
    private scService: ScService,
    private renderer: Renderer2,
    private router: Router,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private analytics: AngularFireAnalytics,
  ) {
    this.afAuth.auth.onAuthStateChanged((auth) => {
      if (auth != null) {
        console.log("auth.email = " + auth.email);
        var user: Observable<User>;
        user = this.afs.doc<User>('users/' + auth.email).valueChanges();
        user.subscribe(_user => {
          this.scService.setCurrUser(_user);
        });

        var account: Observable<Account[]>;
        account = this.afs.collection<Account>('accounts', ref => ref.where('user_key', '==', auth.email)).valueChanges();
        account.subscribe(_accounts => {
          _accounts.forEach(_account => {
            this.scService.setCurrAccount(_account);
            console.log("Account = " + _account.$key);
          });
        });

      } else {
        console.log("auth == null!");
      }
    });
    
    updates.available.subscribe(event => {
      console.log("update available");
      //updates.activateUpdate().then(() => document.location.reload());
      this._update = true;
    });
    // updates.activated.subscribe(event => {
    //   console.log('old version was', event.previous);
    //   console.log('new version is', event.current);
    //   this._update = false;
    // });
    this.route.params.subscribe(params => {
      console.log("Here!");
    });
  }


  login() {
    window.alert("login()! email = " + this.nameEl.nativeElement.value + ", pw = " + this.pwEl.nativeElement.value);
    this.afAuth.auth.signInWithEmailAndPassword(this.nameEl.nativeElement.value, this.pwEl.nativeElement.value)
      .then(value => {
        window.alert(value.user.email);
        // console.log('Nice, it worked!');
        this.router.navigate(['/drawings']);
        // if (this.scService.getDeviceType() != "unknown") {
        //   this.router.navigate(['/mobilemain']);
        // } else {
        //   this.router.navigate(['/mainwin']);
        // }

      })
      .catch(err => {
        console.log('Something went wrong: ', err.message);
      });
  }

  validEmail(email: string): boolean {
    let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    if (email != "" && (email.length <= 5 || !EMAIL_REGEXP.test(email))) {
      return false;
    }
    return true;
  }

  create() {
    window.alert("create()! email = " + this.nameEl.nativeElement.value + ", pw = " + this.pwEl.nativeElement.value);
    if (!this.validEmail(this.nameEl.nativeElement.value)) {
      window.alert("Invalid Email!");
      return;
    }
    firebase.auth().createUserWithEmailAndPassword(this.nameEl.nativeElement.value, this.pwEl.nativeElement.value).catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log("errorCode = " + error.code);
      console.log("errorMessage = " + error.message);
    }).then((user) => {
      if (user) {
        var email: string = user.user.email;
        window.alert("email = " + email);
        var batch = this.afs.firestore.batch();

        var userRef = this.afs.firestore.collection('users').doc(email);
        var userDb: User = new User();
        userDb.$key = email;
        userDb.email = email;
        userDb.name = "David";
        batch.set(userRef, JSON.parse(JSON.stringify(userDb)));

        let acctId = this.afs.createId();
        var acntRef = this.afs.firestore.collection('accounts').doc(acctId);
        var account = new Account();
        account.$key = acctId;
        account.user_key = userDb.$key;
        batch.set(acntRef, JSON.parse(JSON.stringify(account)));

        let projId = this.afs.createId();
        var projRef = this.afs.firestore.collection('projects').doc(projId);
        var project = new Project();
        project.key = projId;
        project.name = "My Project";
        project.accountKey = account.$key;
        batch.set(projRef, JSON.parse(JSON.stringify(project)));

        batch.commit().then(function () {
          window.alert("Account Created!");
        });
      }
    });
  }

  resetPassword() {
    firebase.auth().sendPasswordResetEmail(this.nameEl.nativeElement.value).then(function () {
      window.alert('email sent!');
    }).catch(function (error) {
      console.error("Problem sending password reset email!");
    });
  }

  sendVerification() {
    var user = firebase.auth().currentUser;
    user.sendEmailVerification().then(function () {
      window.alert("Verfication Email Sent!");
    }).catch(function (error) {
      console.error("Problem sending Verfication Email!");
    });
  }

  logout() {
    this.afAuth.auth.signOut();
  }

  public drawingsView() {

  }

  public issuesView() {

  }

  public onResize(e: Event) {

  }

  public onPressUp() {
    window.alert("onPressUp!!!");
  }

  public update() {
    this.scService.getProjects().length = 0;
    if (this.scService.getProjectList().projkeys != null && this.scService.getProjectList().projkeys.length > 0) {
      for (var i = 0; i < this.scService.getProjectList().projkeys.length; i++) {
        this.localStorage.getItem<Project>(this.scService.getProjectList().projkeys[i]).subscribe((_proj) => {
          var proj = _proj as Project;
          this.scService.getProjects().push(proj);
          if (proj.number == null || proj.number.length == 0) {
            proj.number = "Report Number";
          }
          if (proj.name == null || proj.name.length == 0) {
            proj.name = "List Name";
          }
        });
      }
    }
  }

  showSpinner(show: boolean) {
    if(show) {
      this.renderer.setStyle(this.spinnerEl.nativeElement, "display", "inherit");
    } else {
      this.renderer.setStyle(this.spinnerEl.nativeElement, "display", "none");
    }
  }

  public about() {
    const dialogRef = this.dialog.open(AboutdialogComponent, {
      panelClass: 'app-full-bleed-dialog2',
      width: '80vw',
      height: '80vh'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  ngOnInit(): void {
    this.analytics.logEvent('custom_event');
    this.scService.setAppComp(this);
    this.localStorage.getItem<ProjectList>('plist').subscribe((pl_) => {
      var pl = pl_ as ProjectList;
      if (pl == null || !pl.projkeys) {
        console.log("plist does not exist!");
        this.scService.setProjectList(new ProjectList());
        var project: Project = new Project();
        project.name = "New List " + (this.scService.getProjectList().projnos+1);
        project.key = "p" + this.scService.getProjectList().projnos;
        this.scService.getProjectList().projnos = (this.scService.getProjectList().projnos + 1);
        this.scService.getProjectList().projkeys.push(project.key);
        this.localStorage.setItem("plist", this.scService.getProjectList()).subscribe(() => {
          console.log("plist updated!");
        });
        this.localStorage.setItem(project.key, project).subscribe(() => {
          console.log("project added!");
          this.scService.getProjects().push(project);
          this.scService.setCurrProj(project);
          this.router.navigate(['../issues/' + this.scService.getCurrProj().key]);
        });
        this.about();
      } else {
        console.log("loading projectlist length = " + pl.projkeys.length);
        this.scService.setProjectList(pl);
        if (this.scService.getProjectList().projkeys != null && this.scService.getProjectList().projkeys.length > 0) {
          for (var i = 0; i < this.scService.getProjectList().projkeys.length; i++) {
            this.localStorage.getItem<Project>(this.scService.getProjectList().projkeys[i]).subscribe((proj_) => {
              var proj = proj_ as Project;
              this.scService.getProjects().push(proj);
              this.scService.updateProjectList();
              if (proj.number == null || proj.number.length == 0) {
                proj.number = "Report Number";
              }
              if (proj.name == null || proj.name.length == 0) {
                proj.name = "List Name";
              }
            });
          }
          if (this.scService.getProjectList().aboutdiag == null || !this.scService.getProjectList().aboutdiag) {
            this.about();
          }
        } else {
          if (this.scService.getProjectList().aboutdiag == null || !this.scService.getProjectList().aboutdiag) {
            this.about();
          }
        }
      }
    }, (error) => {
      console.error("FieldReport.App ERROR!");
    });
    this.localStorage.getItem<Company>('company').subscribe((comp_) => {
      var comp = comp_ as Company;
      if (comp == null) {
        console.log("company does not exist!");
        this.scService.setCurrComp(new Company());
        this.localStorage.setItem('company', this.scService.getCurrComp()).subscribe(() => {
          console.log("company added!");
        });
      } else {
        console.log("company loaded");
        this.scService.setCurrComp(comp);
      }
    }, (error) => {
      this.scService.setCurrComp(new Company());
      this.localStorage.setItem('company', this.scService.getCurrComp()).subscribe(() => {
        console.log("company added in error method!");
      });
    });
  }
}
