import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule, BUCKET } from '@angular/fire/storage';
import { AngularFireAnalyticsModule, ScreenTrackingService } from '@angular/fire/analytics';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MatNativeDateModule, MatCheckboxModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule} from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { IssuelistitemComponent } from './issues/issuelistitem/issuelistitem.component';
import { PhotograbComponent } from './photograb/photograb.component';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { PhotoComponent } from './issues/issuelistitem/photo/photo.component';
import { ProjdialogComponent } from './dialog/projdialog/projdialog.component';
import { ProjdeleteComponent } from './dialog/projdialog/projdelete/projdelete.component';
import { IssuedeleteComponent } from './issues/issuelistitem/issuedelete/issuedelete.component';
import { PhotodeleteComponent } from './issues/issuelistitem/photo/photodelete/photodelete.component';
import { AboutdialogComponent } from './dialog/aboutdialog/aboutdialog.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { PdfdialogComponent } from './dialog/pdfdialog/pdfdialog.component';
import { ConsoledialogComponent } from './dialog/consoledialog/consoledialog.component';
import { DrawingsviewComponent } from './drawings/drawingsview/drawingsview.component';
import { ScService } from './services/sc.service';
import { DrawingComponent } from './drawings/drawing/drawing.component';
import { StorageModule } from '@ngx-pwa/local-storage';
import { DrawingcanvasComponent } from './drawings/drawingcanvas/drawingcanvas.component';
import { DwgdeleteComponent } from './drawings/drawing/dwgdelete/dwgdelete.component';
import { LongpressDirective } from './directives/longpress.directive';
import { IssueviewComponent } from './issues/issueview/issueview.component';
import { Ng2PicaModule } from 'ng2-pica';
import { IssuelistComponent } from './issues/issuelist/issuelist.component';
import { CompdialogComponent } from './dialog/compdialog/compdialog.component';
import { UseragdialogComponent } from './dialog/aboutdialog/useragdialog/useragdialog.component';
import { UseragsupComponent } from './dialog/aboutdialog/useragsup/useragsup.component';

export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    'press': { time: 600 } // override default settings
  }
}

const routes: Routes = [
  {path: 'drawings/:id/:id2', component: DrawingsviewComponent},
  {path: 'drawings/:id', component: DrawingsviewComponent},
  {path: 'drawings', component: DrawingsviewComponent},
  {path: 'issues/:id/:id2', component: IssuelistComponent},
  {path: 'issues/:id', component: IssuelistComponent},
  {path: 'issues',component: IssuelistComponent},
  {path: 'issue/:id/:id2', component: IssueviewComponent},
  {path: 'issue/:id', component: IssueviewComponent},
  {path: 'issue',component: IssueviewComponent},
  {path: 'dcanvas/:id/:id2', component: DrawingcanvasComponent},
  {path: 'dcanvas/:id', component: DrawingcanvasComponent},
  {path: 'dcanvas',  component: DrawingcanvasComponent},
  {path: 'photograb', component: PhotograbComponent},
  {path: '',component: IssuelistComponent},
];


@NgModule({
  declarations: [
    AppComponent,
    IssuelistitemComponent,
    PhotograbComponent,
    PhotoComponent,
    ProjdialogComponent,
    ProjdeleteComponent,
    IssuedeleteComponent,
    PhotodeleteComponent,
    AboutdialogComponent,
    PdfdialogComponent,
    ConsoledialogComponent,
    DrawingsviewComponent,
    DrawingComponent,
    DrawingcanvasComponent,
    DwgdeleteComponent,
    LongpressDirective,
    IssueviewComponent,
    IssuelistComponent,
    CompdialogComponent,
    UseragdialogComponent,
    UseragsupComponent,
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireAnalyticsModule,
    AngularFireStorageModule,
    BrowserAnimationsModule,
    FormsModule,
    MatMenuModule,
    MatDividerModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatExpansionModule,
    MatGridListModule,
    MatListModule,
    MatSelectModule,
    MatIconModule,
    MatDatepickerModule,
    MatInputModule,
    MatCheckboxModule,
    MatNativeDateModule,
    MatDialogModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    Ng2PicaModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    RouterModule.forRoot(
      routes, { enableTracing: false }
    ),
    StorageModule.forRoot({ IDBNoWrap: true }),
  ],
  providers: [
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig
    },
    {
      provide: BUCKET, 
      useValue: 'pdf_staging' 
    },
    ScService,
    ScreenTrackingService
  ],
  entryComponents: [
    CompdialogComponent,
    PhotograbComponent,
    ProjdialogComponent,
    ProjdeleteComponent,
    IssuedeleteComponent,
    PhotodeleteComponent,
    DwgdeleteComponent,
    AboutdialogComponent,
    UseragdialogComponent,
    UseragsupComponent,
    PdfdialogComponent,
    ConsoledialogComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
