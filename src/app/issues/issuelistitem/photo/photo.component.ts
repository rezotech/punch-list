import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Photo } from 'src/models/photo.model';
import { MatDialog } from '@angular/material';
import { PhotodeleteComponent } from './photodelete/photodelete.component';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css']
})
export class PhotoComponent implements OnInit {
  @Input() photo: Photo;
  @ViewChild("photo_", { read: ElementRef, static: true  }) public photo_el: ElementRef;

  constructor(
    public dialog: MatDialog,
  ) { }

  delPress() {
    console.log("onPress() = " + this.photo.id);
    const dialogRef = this.dialog.open(PhotodeleteComponent, {
      data: { photo: this.photo },
      panelClass: 'app-full-bleed-dialog',
      width: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  ngOnInit() {
    console.log("ngOnInit()");
  }

  ngAfterViewInit() {
    var imgEl: HTMLImageElement = this.photo_el.nativeElement;
    console.log("width,height,scale = " + this.photo.width + "," + this.photo.height + "," + this.photo.scale);
  }

  onLoad(event: any) {
    console.log("image loaded!");
  }

}
