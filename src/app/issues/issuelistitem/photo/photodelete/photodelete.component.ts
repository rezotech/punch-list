import { Component, OnInit, Inject } from '@angular/core';
import { Photo } from 'src/models/photo.model';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ScService } from 'src/app/services/sc.service';

@Component({
  selector: 'app-photodelete',
  templateUrl: './photodelete.component.html',
  styleUrls: ['./photodelete.component.css']
})
export class PhotodeleteComponent implements OnInit {
  photo: Photo = new Photo();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<PhotodeleteComponent>,
    protected localStorage: LocalStorage,
    private scService: ScService,
  ) {
    console.log("photo id = " + data.photo.id);
    this.photo = data.photo;
  }

  ngOnInit() {
  }

  delete() {
    const issue = this.scService.getCurrIssue();
    const index = issue.photokeys.indexOf(this.photo.id, 0);
    if (index > -1) {
      issue.photokeys.splice(index, 1);
    }
    this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
      console.log("project updated!");
    }, (error) => {
      console.error("error updating project.");
    });

    this.localStorage.removeItem('photo-' + this.photo.id).subscribe(() => {
      console.log("photo deleted.");
      if (this.scService.getIssueListItemComp()) {
        this.scService.getIssueListItemComp().update();
      } else if (this.scService.getIssueview()) {
        this.scService.getIssueview().update();
      }
      this.dialogRef.close();
    }, (error) => {
      console.error("error deleting photo.");
    });
  }
}
