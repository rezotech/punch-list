import { Component, OnInit, Inject } from '@angular/core';
import { Issue } from 'src/models/issue.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ScService } from 'src/app/services/sc.service';
import { Drawing } from 'src/models/drawing.model';
import { Dwgmark } from 'src/models/dwgmark.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-issuedelete',
  templateUrl: './issuedelete.component.html',
  styleUrls: ['./issuedelete.component.css']
})
export class IssuedeleteComponent implements OnInit {
  issue: Issue = new Issue();
  isIssueComp: boolean = false;
  issue_no: number = 0;
  issue_description: string = "Issue Description";

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<IssuedeleteComponent>,
    protected localStorage: LocalStorage,
    private router: Router,
    private scService: ScService,
  ) {
    console.log("issue no = " + data.issue.no);
    this.issue = data.issue;
    this.issue_no = this.issue.no;
    this.issue_description = this.issue.description;
    this.isIssueComp = data.isIssueComp;
  }

  ngOnInit() {
  }

  delete() {
    // delete all issue photos. 
    if (this.issue.photokeys != null && this.issue.photokeys.length > 0) {
      for (var j = 0; j < this.issue.photokeys.length; j++) {
        this.localStorage.removeItem('photo-' + this.issue.photokeys[j]).subscribe(() => {
          console.log("photo deleted.");
        }, (error) => {
          console.error("error deleting photo.");
        });
      }
    }

    const project = this.scService.getCurrProj();
    for (var i = 0; i < project.issues.length; i++) {
      const _issue = project.issues[i];
      if (_issue.no > this.issue.no) {
        _issue.no = (_issue.no - 1);
      }
    }


    if (project.drawingkeys) {
      console.log("project has drawings.");
      for (var dwgkey of project.drawingkeys) {
        var dwgmarks: Dwgmark[] = [];
        this.localStorage.getItem("dwg-" + dwgkey).subscribe((dwg_) => {
          var dwg: Drawing = dwg_ as Drawing;
          console.log("going to check for dwgmark");
          for (var dwgmark of dwg.dwgmarks) {
            console.log("dwgmark on issueno = " + dwgmark.issue_no);
            if (dwgmark.issue_no != this.issue.no) {
              if (dwgmark.issue_no > this.issue.no) {
                dwgmark.issue_no = (dwgmark.issue_no-1);
              }
              dwgmarks.push(dwgmark);
            }
          }
          dwg.dwgmarks = dwgmarks;
          this.localStorage.setItem("dwg-" + dwgkey, dwg).subscribe(() => {
            console.log("Drawing updated!");
          });
        });
      }
    }
  
    const index = project.issues.indexOf(this.issue, 0);
    if (index > -1) {
      project.issues.splice(index, 1);
    }
    this.localStorage.setItem(project.key, project).subscribe(() => {
      console.log("project updated!");
      this.scService.getIssueListComp().updateIssues();

    }, (error) => {
      console.error("error updating project.");
    });
    this.dialogRef.close();

    if(!this.isIssueComp) {
      this.router.navigate(['../issues/' + this.scService.getCurrProj().key]);
    } else {
      this.router.navigate(['../dcanvas/' + this.scService.getCurrProj().key + "/" + this.scService.getCurrDwg().id]);
    }
      }

}
