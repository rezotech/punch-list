import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssuedeleteComponent } from './issuedelete.component';

describe('IssuedeleteComponent', () => {
  let component: IssuedeleteComponent;
  let fixture: ComponentFixture<IssuedeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssuedeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuedeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
