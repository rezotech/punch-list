import { Component, ElementRef, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ScService } from 'src/app/services/sc.service';
import { Issue } from 'src/models/issue.model';
import { Photo } from 'src/models/photo.model';
import { IssuedeleteComponent } from './issuedelete/issuedelete.component';

@Component({
  selector: 'app-issuelistitem',
  templateUrl: './issuelistitem.component.html',
  styleUrls: ['./issuelistitem.component.css']
})
export class IssuelistitemComponent implements OnInit {
  @Input() issue: Issue;
  @ViewChild("photoph", { read: ElementRef, static: true }) photophEl: ElementRef;
  @ViewChild("fileinput", { read: ElementRef, static: true }) fileinputEl: ElementRef;
  photos: Photo[] = [];
  date: FormControl;
  options: string[] = [ "Mech", "Elect", "Plumb", "Fire" ];

  constructor(
    public dialog: MatDialog,
    protected localStorage: LocalStorage,
    private scService: ScService,
    private renderer: Renderer2,
    private route: ActivatedRoute,
    private router: Router,
  ) 
  {}

  async addPhoto2(e: any) {
    this.scService.getAppComp().showSpinner(true);
    await this.getOrientation(e.target.files[0], async (orientation) => {
      // window.alert("orientation = " + orientation);
      if (orientation >= 2 && orientation <= 8) {
        var fr = new FileReader;
        fr.onload = (async () => {
          var dataUrl = fr.result.toString();
          await this.resetOrientation(dataUrl, orientation, async (resetBase64Image) => {
            this.addPhoto2a(resetBase64Image);
          });
        });
        fr.readAsDataURL(e.target.files[0]);
      } else {
        var fr = new FileReader;
        fr.onload = (() => {
          var dataUrl = fr.result.toString();
          this.addPhoto2a(dataUrl);
        });
        fr.readAsDataURL(e.target.files[0]);
      }
    });
  }

  async addPhoto2a(dataUrl) {
    var photo = new Photo;
    var img = new Image;
    photo.dataUrl = dataUrl;

    img.src = photo.dataUrl;
    img.onload = (async () => {
      // window.alert(img.width + "," + img.height); // image is loaded; sizes are available
      photo.scale = this.scService.getImgQual();
      console
      photo.width = (img.width*photo.scale)*photo.scale;
      photo.height = (img.height*photo.scale)*photo.scale;
      photo.id = new Date().getTime() + "";
      // var width = (window.innerWidth*.75);
      // var height = (window.innerHeight*.75);
      // photo.scale = Math.min((width / photo.width), (height / photo.height));
      // window.alert(fr.result);

      var canvas1 = document.createElement('canvas');
      canvas1.width = img.width * this.scService.getCurrProj().img_qual;
      canvas1.height = img.height * this.scService.getCurrProj().img_qual;
      canvas1.getContext("2d").drawImage(img, 0, 0, canvas1.width, canvas1.height);

      var img2 = new Image;
      img2.src = canvas1.toDataURL("image/png");

      img2.onload = (async () => {
        canvas1 = document.createElement('canvas');
        canvas1.width = img2.width * this.scService.getImgQual();
        canvas1.height = img2.height * this.scService.getImgQual();
        canvas1.getContext("2d").drawImage(img, 0, 0, canvas1.width, canvas1.height);

        photo.dataUrl = canvas1.toDataURL("image/jpg", .5);
  
        if (this.scService.getCurrIssue().photokeys == null) {
          this.scService.getCurrIssue().photokeys = [];
        }
        this.scService.getCurrIssue().photokeys.push(photo.id);
        this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
          console.log("project updated!")
        });
        this.localStorage.setItem("photo-" + photo.id, photo).subscribe(() => {
          console.log("photo saved!");
          this.update();
        });
        this.fileinputEl.nativeElement.value = "";
      });
    });
  }

  public issueView() {
    this.router.navigate(['../issue/' + this.scService.getCurrProj().key + "/" + this.issue.no]);
  }

  public setIssueListComp() {
    console.log("setIssueListComp()");
    // this.router.navigate(['../issues/' + this.scService.getCurrProj().key + "/" + this.issue.no]);
    this.scService.setIssueListItemComp(this);
    // this.update();
  }

  public tradeBlur(event: any) {
    console.log("tradeBlur");
    this.issue.trade = event.target.value;
    this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
      console.log("trade updated!");
    });
  }

  public floorBlur(event: any) {
    console.log("floorBlur");
    this.issue.floor = event.target.value;
    this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
      console.log("floor updated!");
    });
  }

  public locationBlur(event: any) {
    console.log("locationBlur");
    this.issue.location = event.target.value;
    this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
      console.log("location updated!");
    });
  }

  public descriptionBlur(event: any) {
    console.log("descriptionBlur");
    this.issue.description = event.target.value;
    this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
      console.log("description updated!");
    });
  }

  public dateBlur(src: string, event: MatDatepickerInputEvent<Date>) {
    console.log("dateBlur");
    this.issue.open_date = new Date(event.value).getTime();
    this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
      console.log("date updated!");
    });
  }

  public update() {
    console.log("update() this.issue.photokeys.length = " +this.issue.photokeys.length);
    this.photos.length = 0;;
    if (this.issue.photokeys != null && this.issue.photokeys.length > 0) {
      this.photos = [];
      for (var i = 0; i < this.issue.photokeys.length; i++) {
        this.localStorage.getItem<Photo>("photo-" + this.issue.photokeys[i]).subscribe((photo_) => {
          var photo = photo_ as Photo;
          var width = (window.innerWidth-40)/2;
          var height = (window.innerHeight-40)/2;
          var scale = Math.min(width / photo.width, height / photo.height);
          console.log("img.width, window.width = " + photo.width + "," + window.innerWidth);
          console.log("scale=" + scale);
          photo.width = photo.width * scale;
          photo.height = photo.height * scale;
          this.photos.push(photo);
        }, (error) => {
          console.log("Error getting photo!");
        });
        this.scService.getAppComp().showSpinner(false);
      }
    }
  }

  public delPress() {
    console.log("onPress() = " + this.issue.no);
    const dialogRef = this.dialog.open(IssuedeleteComponent, {
      data: { issue: this.issue, isIssueComp: false },
      panelClass: 'app-full-bleed-dialog',
      width: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  getOrientation = (file: File, callback: Function) => {
    var reader = new FileReader();

    reader.onload = (event: ProgressEvent) => {

      if (!event.target) {
        return;
      }

      const file = event.target as FileReader;
      const view = new DataView(file.result as ArrayBuffer);

      if (view.getUint16(0, false) != 0xFFD8) {
        return callback(-2);
      }

      const length = view.byteLength
      let offset = 2;

      while (offset < length) {
        if (view.getUint16(offset + 2, false) <= 8) return callback(-1);
        let marker = view.getUint16(offset, false);
        offset += 2;

        if (marker == 0xFFE1) {
          if (view.getUint32(offset += 2, false) != 0x45786966) {
            return callback(-1);
          }

          let little = view.getUint16(offset += 6, false) == 0x4949;
          offset += view.getUint32(offset + 4, little);
          let tags = view.getUint16(offset, little);
          offset += 2;
          for (let i = 0; i < tags; i++) {
            if (view.getUint16(offset + (i * 12), little) == 0x0112) {
              return callback(view.getUint16(offset + (i * 12) + 8, little));
            }
          }
        } else if ((marker & 0xFF00) != 0xFF00) {
          break;
        }
        else {
          offset += view.getUint16(offset, false);
        }
      }
      return callback(-1);
    };
    reader.readAsArrayBuffer(file);
  }

  async resetOrientation(srcBase64, srcOrientation, callback) {
    var img = new Image();

    img.onload = (async () => {
      var width = img.width,
        height = img.height,
        canvas = document.createElement('canvas'),
        ctx = canvas.getContext("2d");

      // set proper canvas dimensions before transform & export
      if (4 < srcOrientation && srcOrientation < 9) {
        canvas.width = height;
        canvas.height = width;
      } else {
        canvas.width = width;
        canvas.height = height;
      }

      // transform context before drawing image
      switch (srcOrientation) {
        case 2: ctx.transform(-1, 0, 0, 1, width, 0); break;
        case 3: ctx.transform(-1, 0, 0, -1, width, height); break;
        case 4: ctx.transform(1, 0, 0, -1, 0, height); break;
        case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
        case 6: ctx.transform(0, 1, -1, 0, height, 0); break;
        case 7: ctx.transform(0, -1, -1, 0, height, width); break;
        case 8: ctx.transform(0, -1, 1, 0, 0, width); break;
        default: break;
      }

      // draw image
      ctx.drawImage(img, 0, 0);

      // export base64
      callback(canvas.toDataURL());
    });

    img.src = srcBase64;
  }

  ngOnInit() {
    this.date = new FormControl(new Date(this.issue.open_date));
    this.update();
  }

}
