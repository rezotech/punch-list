import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssuelistitemComponent } from './issuelistitem.component';

describe('IssuelistitemComponent', () => {
  let component: IssuelistitemComponent;
  let fixture: ComponentFixture<IssuelistitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssuelistitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuelistitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
