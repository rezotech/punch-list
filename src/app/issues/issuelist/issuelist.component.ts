import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { AboutdialogComponent } from 'src/app/dialog/aboutdialog/aboutdialog.component';
import { CompdialogComponent } from 'src/app/dialog/compdialog/compdialog.component';
import { ConsoledialogComponent } from 'src/app/dialog/consoledialog/consoledialog.component';
import { ProjdialogComponent } from 'src/app/dialog/projdialog/projdialog.component';
import { PdfService } from 'src/app/services/pdf.service';
import { ScService } from 'src/app/services/sc.service';
import { Company } from 'src/models/company.model';
import { Issue } from 'src/models/issue.model';
import { Project } from 'src/models/project.model';

@Component({
  selector: 'app-issuelist',
  templateUrl: './issuelist.component.html',
  styleUrls: ['./issuelist.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class IssuelistComponent implements OnInit, AfterViewInit {
  @ViewChild("timer", { read: ElementRef, static: true }) timerEl: ElementRef;
  @ViewChild(MatPaginator, { read: ElementRef, static: true }) paginator: MatPaginator;
  columnsToDisplay = ['no', 'trade', 'floor', 'location'];
  public rawissues: Issue[] = [];
  public filteredissues: Issue[] = [];
  public dataSource: Issue[];
  public dataSource2: MatTableDataSource<Issue>;
  public projects: Project[] = [];

  public pageIndex: number = 0;
  private pageSize: number = 10;
  public projname: string = "";
 
  constructor(
    protected localStorage: LocalStorage,
    private route: ActivatedRoute,
    private router: Router,
    private scService: ScService,
    private pdfService: PdfService,
    public dialog: MatDialog,
    private renderer: Renderer2,
  ) {
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.scService.getAppComp().showSpinner(true);
        this.localStorage.getItem<Project>(params['id']).subscribe((proj_) => {
          var proj = proj_ as Project;
          this.scService.setCurrProj(proj);
          this.projname = proj.name;
          this.updateIssues();
        });
      }
    });

    this.scService.project_messageSource.subscribe((proj => {
      if (proj != null) {
        this.projname = proj.name;
        for(var p_ of this.projects) {
          if(p_.key==proj.key) {
            p_.name = proj.name;
            break;
          }
        }
      }
    }));
  }

  ngOnInit() {
    this.scService.setIssueListComp(this);
    // if (this.scService.getProjectList().projkeys == null) {
    //   this.addList();
    // }
    this.projects = this.scService.getProjects();
    console.log("projects.length = " + this.projects.length);
  }

  ngAfterViewInit(): void {
  }

  public issuesView() {
    this.router.navigate(['../issues/' + this.scService.getCurrProj().key]);
  }

  public drawingsView() {
    this.router.navigate(['../drawings/' + this.scService.getCurrProj().key]);
  }

  public addList() {
    if (this.scService.getProjectList().projkeys == null) {
      this.scService.getProjectList().projkeys = [];
    }
    var project: Project = new Project();
    project.name = "New List " + (this.scService.getProjectList().projnos + 1);
    project.key = "p" + this.scService.getProjectList().projnos;
    this.scService.getProjectList().projnos = (this.scService.getProjectList().projnos + 1);
    this.scService.getProjectList().projkeys.push(project.key);
    this.localStorage.setItem("plist", this.scService.getProjectList()).subscribe(() => {
      console.log("projkey added!");
    });
    this.localStorage.setItem(project.key, project).subscribe(() => {
      console.log("project added!");
      this.scService.getProjects().push(project);
    });
  }

  public listSettings() {
    if (this.scService.getCurrProj() == null) {
      return;
    }
    const dialogRef = this.dialog.open(ProjdialogComponent, {
      data: { project: this.scService.getCurrProj() },
      panelClass: 'app-full-bleed-dialog',
      width: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  public companySettings() {
    if (this.scService.getCurrComp() == null) {
      this.scService.setCurrComp(new Company());
    }
    const dialogRef = this.dialog.open(CompdialogComponent, {
      data: { company: this.scService.getCurrComp() },
      panelClass: 'app-full-bleed-dialog',
      width: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  public async pdfIt() {
    if (this.scService.getCurrProj() == null) {
      return;
    }
    this.setTimer(true);
    this.pdfService.pdfIt();
    return;
  }
  
  public about() {
    const dialogRef = this.dialog.open(AboutdialogComponent, {
      panelClass: 'app-full-bleed-dialog2',
      width: '80vw',
      height: '80vh'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  public console() {
    const dialogRef = this.dialog.open(ConsoledialogComponent, {
      panelClass: 'app-full-bleed-dialog2',
      width: '400px',
      height: '80vh'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

 
  public setTimer(timer: boolean) {
    if (timer) {
      this.renderer.setStyle(this.timerEl.nativeElement, "display", "inline");
    } else {
      this.renderer.setStyle(this.timerEl.nativeElement, "display", "none");
    }
  }

  public addIssue() {
    if (this.scService.getCurrProj() == null) {
      return;
    }
    let issue: Issue = new Issue();
    issue.no = (this.scService.getCurrProj().issues.length + 1) * 1;
    issue.open_date = new Date().getTime();
    this.scService.getCurrProj().issues.push(issue);
    this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
      console.log("issue added! #" + this.scService.getCurrProj().issues.length);
      this.rawissues = this.scService.getCurrProj().issues;
      this.pageIndex = Math.floor((issue.no-1)/this.pageSize);
      // this.paginator.pageIndex = this.pageIndex;
      console.log("issue.no=" + issue.no + ", pageSize=" + this.pageSize);
      console.log("pageIndex=" + this.pageIndex);
      // if(issue.no>((this.pageIndex*this.pageSize)+this.pageSize)) {
      //   console.log("increment pageIndex");
      //   this.pageIndex++;
      // }
      this.filteredissues = this.rawissues.slice(this.pageIndex*this.pageSize, ((this.pageIndex*this.pageSize)+this.pageSize));
      this.dataSource = [...this.filteredissues];
    });
  }

  public testPress(issue: Issue) {
    console.log("testPres()!!! issue no = " + issue.no);
  }

  public currentIssue(issue: Issue) {
    console.log("Current Issue!");
    this.scService.setCurrIssue(issue);
  }

  public updateIssues() {
    console.log("updateIssues()");
    // this.issues.length = 0;

    this.rawissues = this.scService.getCurrProj().issues;
    this.filteredissues = this.rawissues.slice(this.pageIndex*this.pageSize, ((this.pageIndex*this.pageSize)+this.pageSize));
    this.dataSource = [...this.filteredissues];
    console.log("updateIssues() project = " + this.scService.getCurrProj().name);
    console.log("updateIssues() count = " + this.rawissues.length);
    this.scService.getAppComp().showSpinner(false);
  }

  public getNext(e: any) {
    console.log(e);
    console.log(e.previousPageIndex);
    console.log(e.pageIndex);
    console.log(e.pageSize);
    console.log(e.length);
    this.pageIndex = e.pageIndex;
    this.filteredissues = this.rawissues.slice(e.pageIndex*10, ((e.pageIndex*10)+10));
    this.dataSource = [...this.filteredissues];
  }
}