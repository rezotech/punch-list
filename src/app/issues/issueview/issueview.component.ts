import { Component, OnInit, Input, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { Issue } from 'src/models/issue.model';
import { MatDialog } from '@angular/material/dialog';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { Photo } from 'src/models/photo.model';
import { ScService } from 'src/app/services/sc.service';
import { FormControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { IssuedeleteComponent } from '../issuelistitem/issuedelete/issuedelete.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from 'src/models/project.model';
import { ConsoledialogComponent } from 'src/app/dialog/consoledialog/consoledialog.component';
import { AboutdialogComponent } from 'src/app/dialog/aboutdialog/aboutdialog.component';
import { CompdialogComponent } from 'src/app/dialog/compdialog/compdialog.component';
import { Company } from 'src/models/company.model';
import { ProjdialogComponent } from 'src/app/dialog/projdialog/projdialog.component';
import { PdfService } from 'src/app/services/pdf.service';

@Component({
  selector: 'app-issueview',
  templateUrl: './issueview.component.html',
  styleUrls: ['./issueview.component.css']
})
export class IssueviewComponent implements OnInit {
  issue: Issue = new Issue();
  @ViewChild("timer", { read: ElementRef, static: true }) timerEl: ElementRef;
  @ViewChild("photoph", { read: ElementRef, static: true  }) photophEl: ElementRef;
  @ViewChild("fileinput", { read: ElementRef, static: true }) fileinputEl: ElementRef;
  photos: Photo[] = [];
  options: string[] = [ "Mech", "Elect", "Plumb", "Fire" ];
  date: FormControl = new FormControl();
 
  constructor(
    public dialog: MatDialog,
    protected localStorage: LocalStorage,
    private scService: ScService,
    private renderer: Renderer2,
    private pdfService: PdfService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.route.params.subscribe(params => {
      if (params['id']) {
        console.log("id = " + params['id']);
        this.localStorage.getItem<Project>(params['id']).subscribe((proj_) => {
          var proj = proj_ as Project;
          this.scService.setCurrProj(proj);
          if(params['id2']) {
            console.log("id2=" + params['id2']);
            for(var issue_ of this.scService.getCurrProj().issues) {
              if(issue_.no === (params['id2']*1)) {
                this.issue = issue_;
                this.date = new FormControl(new Date(this.issue.open_date));
                this.update();
              }
            }
          }
        });
      }
    });
   }

   async addPhoto2(e: any) {
    await this.getOrientation(e.target.files[0], async (orientation) => {
      // window.alert("orientation = " + orientation);
      if (orientation >= 2 && orientation <= 8) {
        var fr = new FileReader;
        fr.onload = (async () => {
          var dataUrl = fr.result.toString();
          await this.resetOrientation(dataUrl, orientation, async (resetBase64Image) => {
            this.addPhoto2a(resetBase64Image);
          });
        });
        fr.readAsDataURL(e.target.files[0]);
      } else {
        var fr = new FileReader;
        fr.onload = (() => {
          var dataUrl = fr.result.toString();
          this.addPhoto2a(dataUrl);
        });
        fr.readAsDataURL(e.target.files[0]);
      }
    });
  }

  async addPhoto2a(dataUrl) {
    var photo = new Photo;
    var img = new Image;
    photo.dataUrl = dataUrl;

    img.src = photo.dataUrl;
    img.onload = (async () => {
      // window.alert(img.width + "," + img.height); // image is loaded; sizes are available
      photo.scale = this.scService.getImgQual();
      photo.width = (img.width*photo.scale)*photo.scale;
      photo.height = (img.height*photo.scale)*photo.scale;
      photo.id = new Date().getTime() + "";
      // var width = (window.innerWidth*.75);
      // var height = (window.innerHeight*.75);
      // photo.scale = Math.min((width / photo.width), (height / photo.height));
      // window.alert(fr.result);

      var canvas1 = document.createElement('canvas');
      canvas1.width = img.width * this.scService.getCurrProj().img_qual;
      canvas1.height = img.height * this.scService.getCurrProj().img_qual;
      canvas1.getContext("2d").drawImage(img, 0, 0, canvas1.width, canvas1.height);

      var img2 = new Image;
      img2.src = canvas1.toDataURL("image/png");

      img2.onload = (async () => {
        canvas1 = document.createElement('canvas');
        canvas1.width = img2.width * this.scService.getImgQual();
        canvas1.height = img2.height * this.scService.getImgQual();
        canvas1.getContext("2d").drawImage(img, 0, 0, canvas1.width, canvas1.height);

        photo.dataUrl = canvas1.toDataURL("image/jpg", .5);
  
        if (this.scService.getCurrIssue().photokeys == null) {
          this.scService.getCurrIssue().photokeys = [];
        }
        this.scService.getCurrIssue().photokeys.push(photo.id);
        this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
          console.log("project updated!")
        });
        this.localStorage.setItem("photo-" + photo.id, photo).subscribe(() => {
          console.log("photo saved scale = " + photo.scale);
          this.update();
        });
        this.fileinputEl.nativeElement.value = "";
      });
    });
  }

  public issuesView() {
    this.router.navigate(['../issues/' + this.scService.getCurrProj().key]);
  }

  public drawingsView() {
    this.router.navigate(['../drawings/' + this.scService.getCurrProj().key]);
  }

  public addList() {
    if (this.scService.getProjectList().projkeys == null) {
      this.scService.getProjectList().projkeys = [];
    }
    var project: Project = new Project();
    project.name = "New List " + this.scService.getProjectList().projnos;
    project.key = "p" + this.scService.getProjectList().projnos;
    this.scService.getProjectList().projnos = (this.scService.getProjectList().projnos + 1);
    this.scService.getProjectList().projkeys.push(project.key);
    this.localStorage.setItem("plist", this.scService.getProjectList()).subscribe(() => {
      console.log("projkey added!");
    });
    this.localStorage.setItem(project.key, project).subscribe(() => {
      console.log("project added!");
      this.scService.getProjects().push(project);
    });
  }

  public listSettings() {
    if (this.scService.getCurrProj() == null) {
      return;
    }
    const dialogRef = this.dialog.open(ProjdialogComponent, {
      data: { project: this.scService.getCurrProj() },
      panelClass: 'app-full-bleed-dialog',
      width: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  public companySettings() {
    if (this.scService.getCurrComp() == null) {
      this.scService.setCurrComp(new Company());
    }
    const dialogRef = this.dialog.open(CompdialogComponent, {
      data: { company: this.scService.getCurrComp() },
      panelClass: 'app-full-bleed-dialog',
      width: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  public async pdfIt() {
    if (this.scService.getCurrProj() == null) {
      return;
    }
    this.setTimer(true);
    this.pdfService.pdfIt();
    return;
  }
  
  public about() {
    const dialogRef = this.dialog.open(AboutdialogComponent, {
      panelClass: 'app-full-bleed-dialog2',
      width: '400px',
      height: '80vh'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  public console() {
    const dialogRef = this.dialog.open(ConsoledialogComponent, {
      panelClass: 'app-full-bleed-dialog2',
      width: '400px',
      height: '80vh'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  public setTimer(timer: boolean) {
    if (timer) {
      this.renderer.setStyle(this.timerEl.nativeElement, "display", "inline");
    } else {
      this.renderer.setStyle(this.timerEl.nativeElement, "display", "none");
    }
  }
  
  public tradeBlur(event: any) {
    console.log("tradeBlur");
    this.issue.trade = event.target.value;
    this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
      console.log("trade updated!");
    });
  }

  public floorBlur(event: any) {
    console.log("floorBlur");
    this.issue.floor = event.target.value;
    this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
      console.log("floor updated!");
    });
  }

  public locationBlur(event: any) {
    console.log("locationBlur");
    this.issue.location = event.target.value;
    this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
      console.log("location updated!");
    });
  }

  public descriptionBlur(event: any) {
    console.log("descriptionBlur");
    this.issue.description = event.target.value;
    this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
      console.log("description updated!");
    });
  }

  public dateBlur(src: string, event: MatDatepickerInputEvent<Date>) {
    console.log("dateBlur");
    this.issue.open_date = new Date(event.value).getTime();
    this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
      console.log("date updated!");
    });
  }

  public update() {
    this.photos.length = 0;;
    if (this.issue.photokeys != null && this.issue.photokeys.length>0) {
      this.photos = [];
      for (var i = 0; i < this.issue.photokeys.length; i++) {
        this.localStorage.getItem<Photo>("photo-" + this.issue.photokeys[i]).subscribe((photo_) => {
          var photo = photo_ as Photo;
          var width = (window.innerWidth-40)/2;
          var height = (window.innerHeight-40)/2;
          var scale = Math.min(width / photo.width, height / photo.height);
          console.log("img.width, window.width = " + photo.width + "," + window.innerWidth);
          console.log("scale=" + scale);
          photo.width = photo.width * scale;
          photo.height = photo.height * scale;
          this.photos.push(photo as Photo);
        }, (error) => {
          console.log("Error getting photo!");
        });
      }
    }
    this.scService.setCurrIssue(this.issue);
  }

  public delPress() {
    console.log("onPress() = " + this.issue.no);
    const dialogRef = this.dialog.open(IssuedeleteComponent, {
      data: { issue: this.issue, isIssueComp: true },
      panelClass: 'app-full-bleed-dialog',
      width: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
  }

  getOrientation = (file: File, callback: Function) => {
    var reader = new FileReader();

    reader.onload = (event: ProgressEvent) => {

      if (!event.target) {
        return;
      }

      const file = event.target as FileReader;
      const view = new DataView(file.result as ArrayBuffer);

      if (view.getUint16(0, false) != 0xFFD8) {
        return callback(-2);
      }

      const length = view.byteLength
      let offset = 2;

      while (offset < length) {
        if (view.getUint16(offset + 2, false) <= 8) return callback(-1);
        let marker = view.getUint16(offset, false);
        offset += 2;

        if (marker == 0xFFE1) {
          if (view.getUint32(offset += 2, false) != 0x45786966) {
            return callback(-1);
          }

          let little = view.getUint16(offset += 6, false) == 0x4949;
          offset += view.getUint32(offset + 4, little);
          let tags = view.getUint16(offset, little);
          offset += 2;
          for (let i = 0; i < tags; i++) {
            if (view.getUint16(offset + (i * 12), little) == 0x0112) {
              return callback(view.getUint16(offset + (i * 12) + 8, little));
            }
          }
        } else if ((marker & 0xFF00) != 0xFF00) {
          break;
        }
        else {
          offset += view.getUint16(offset, false);
        }
      }
      return callback(-1);
    };
    reader.readAsArrayBuffer(file);
  }

  async resetOrientation(srcBase64, srcOrientation, callback) {
    var img = new Image();

    img.onload = (async () => {
      var width = img.width,
        height = img.height,
        canvas = document.createElement('canvas'),
        ctx = canvas.getContext("2d");

      // set proper canvas dimensions before transform & export
      if (4 < srcOrientation && srcOrientation < 9) {
        canvas.width = height;
        canvas.height = width;
      } else {
        canvas.width = width;
        canvas.height = height;
      }

      // transform context before drawing image
      switch (srcOrientation) {
        case 2: ctx.transform(-1, 0, 0, 1, width, 0); break;
        case 3: ctx.transform(-1, 0, 0, -1, width, height); break;
        case 4: ctx.transform(1, 0, 0, -1, 0, height); break;
        case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
        case 6: ctx.transform(0, 1, -1, 0, height, 0); break;
        case 7: ctx.transform(0, -1, -1, 0, height, width); break;
        case 8: ctx.transform(0, -1, 1, 0, 0, width); break;
        default: break;
      }

      // draw image
      ctx.drawImage(img, 0, 0);

      // export base64
      callback(canvas.toDataURL());
    });

    img.src = srcBase64;
  }

  ngOnInit() {
    this.scService.setIssueview(this);
  }

}