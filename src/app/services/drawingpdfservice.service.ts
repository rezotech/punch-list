import { Injectable } from '@angular/core';
import { Drawing } from 'src/models/drawing.model';
import { Dwgmark } from 'src/models/dwgmark.model';

declare var PDFDocument: any;
declare var blobStream: any;
declare const fabric: any;

@Injectable({
  providedIn: 'root'
})
export class DrawingpdfserviceService {
  private dwgmarkImage: any;
  private doc: any;
  private dwg: any;

  constructor() { }

  async pdfIt(dwg_: Drawing) {
    console.log("pdfIt!");
    this.dwg = dwg_;

    this.doc = new PDFDocument({
      margin: 0,
      autoFirstPage: false,
      layout: "landscape"
    });
    this.doc.info['Title'] = "drawing.pdf";

    this.doc.addPage({
      layout: "landscape",
      margin: 0
    });

    var base64str = this.dwg.dataUrl.replace(/^data:image\/[a-z]+;base64,/, "");
    while (base64str.length % 4 > 0) {
      base64str += '=';
    }
    base64str = "data:image/png;base64," + base64str;

    var width = 750;
    var height = 500;
    this.dwg.scale_ = Math.min((width / this.dwg.width), (height / this.dwg.height));

    // var xOff = (792-(this.dwg.width * this.dwg.scale_))/2/2;
    // var yOff = (612-(this.dwg.height * this.dwg.scale_))/2/2;

    this.doc.image(base64str, 0, 0, { fit: [750, 500] });

    this.loadImage("assets/map-marker-2-32.png").then((img) => {
      console.log("Preload ", img);
      this.dwgmarkImage = img;
      for (var dwgmark of this.dwg.dwgmarks) {
        this.createImage(dwgmark);
      }
      this.close();
    });

    this.doc.circle(0, 0, 8).lineWidth(2).stroke("yellow");
    
  }

  createImage(dwgmark: Dwgmark) {   // load an image function 
    // creates a new i each time it is called
    var canvas = new fabric.Canvas('thecanvas');
    canvas.width = 128;
    canvas.height = 128;
    var imageInstance = new fabric.Image(this.dwgmarkImage, {
      left: 0,
      top: 0,
      angle: 0,
      opacity: 1,
      // width: (64),
      // height: (64),
    });
    canvas.add(imageInstance);
    var dataURL = canvas.toDataURL("image/png");
    // console.log("dataURL=" + dataURL);
    var x_ = (dwgmark.x*this.dwg.width*this.dwg.scale)-28;
    var y_ = (dwgmark.y*this.dwg.height*this.dwg.scale)-32;
    console.log("x_,y_=" + dwgmark.x + "," + dwgmark.y);
    this.doc.fontSize(14);
    var string_width = this.doc.widthOfString("#" + dwgmark.issue_no);
    this.doc.image(dataURL, (x_+string_width), y_, { scale: 1 });
    this.doc.text("#" + dwgmark.issue_no, x_, y_, { width: string_width, align: 'right' });
    // this.doc.circle((x_), (y_), 8).lineWidth(2).stroke("yellow");
    // this.doc.rect(0,0,x_,y_).lineWidth(1).stroke("black");
  }

  loadImage(src: string) {
    return new Promise((resolve, reject) => {
      console.log(src);
      var img = new Image();
      img.onload = () => resolve(img);
      img.src = src;
    });
  }

  close() {
    var stream = this.doc.pipe(blobStream())
    this.doc.end();
    stream.on("finish", () => {
      // var blob = stream.toBlob('application/pdf');
      var url = stream.toBlobURL('application/pdf');
      window.open(url, "drawing.pdf");
    });
  }
}
