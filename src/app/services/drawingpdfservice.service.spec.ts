import { TestBed } from '@angular/core/testing';

import { DrawingpdfserviceService } from './drawingpdfservice.service';

describe('DrawingpdfserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DrawingpdfserviceService = TestBed.get(DrawingpdfserviceService);
    expect(service).toBeTruthy();
  });
});
