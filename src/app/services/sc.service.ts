import { Injectable } from '@angular/core';
import { Project } from 'src/models/project.model';
import { Issue } from 'src/models/issue.model';
import { IssuelistitemComponent } from '../issues/issuelistitem/issuelistitem.component';
import { PhotograbComponent } from '../photograb/photograb.component';
import { AppComponent } from '../app.component';
import { ProjectList } from 'src/models/projectlist.model';
import { ConsoledialogComponent } from '../dialog/consoledialog/consoledialog.component';
import { Drawing } from 'src/models/drawing.model';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { DrawingsviewComponent } from '../drawings/drawingsview/drawingsview.component';
import { IssuelistComponent } from '../issues/issuelist/issuelist.component';
import { Company } from 'src/models/company.model';
import { IssueviewComponent } from '../issues/issueview/issueview.component';
import { BehaviorSubject } from 'rxjs';
import { Account } from 'src/models/account.model';
import { User } from 'src/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class ScService {
  private projectList: ProjectList = null;
  private currUser: User = null;
  private currAccount: Account = null;
  private currComp: Company = null;
  private currProj: Project = null;
  private currIssue: Issue = null;
  private currDwg: Drawing = null;
  private appComp: AppComponent;
  private issueViewComp: IssueviewComponent;
  private issueList2Comp: IssuelistComponent;
  private issueListItemComp: IssuelistitemComponent;
  private drawingsViewComp: DrawingsviewComponent;
  private photograbComp: PhotograbComponent;
  private consoleDialog: ConsoledialogComponent;
  public projects: Project[] = [];
  public dwgs: Drawing[] = [];

  user_messageSource = new BehaviorSubject<User>(null);
  account_messageSource = new BehaviorSubject<Account>(null);
  project_messageSource = new BehaviorSubject<Project>(null);
  projects_messageSource = new BehaviorSubject(null);

  public lowimgQual = .2;
  public midimgQual = .3;
  public higimgQual = .4;


  constructor(
    protected localStorage: LocalStorage,
  ) { }

  public getImgQual() {
    if (this.currProj) {
      if (this.currProj.img_qual) {
        if (this.currProj.img_qual == .75) {
          return .3;
        } else {
          return this.currProj.img_qual;
        }
      } else {
        this.currProj.img_qual = .3;
        this.localStorage.setItem(this.currProj.key, this.currProj).subscribe(() => {
          console.log("project updated!")
        });
      }
    }
  }

  setCurrUser(user: User) {
    if (user == null) {
      return;
    }
    this.currUser = user;
    this.user_messageSource.next(user);
    // console.log("setCurrUser() = " + user.$key);
  }

  getCurrUser() {
    return this.currUser;
  }

  setCurrAccount(account: Account) {
    this.currAccount = account;
    this.account_messageSource.next(account);
    // console.log("setCurrAccount() = " + account.$key);
  }

  getCurrAccount() {
    return this.currAccount;
  }

  public setProjectList(projList: ProjectList) {
    this.projectList = projList;
  }

  public getProjectList() {
    return this.projectList;
  }

  public setProjects(projs: Project[]) {
    this.projects = projs;
  }

  public getProjects() {
    return this.projects;
  }

  public setDrawings(dwgs_: Drawing[]) {
    this.dwgs = dwgs_;
  }

  public getDrawings() {
    return this.dwgs;
  }

  public setAppComp(appComp: AppComponent) {
    this.appComp = appComp;
  }

  public getAppComp() {
    return this.appComp;
  }

  public setPhotograbComp(photograbComp: PhotograbComponent) {
    this.photograbComp = photograbComp;
  }

  public getPhotograbComp() {
    return this.photograbComp;
  }

  public setIssueview(issueView: IssueviewComponent) {
    this.issueViewComp = issueView;
  }

  public getIssueview() {
    return this.issueViewComp;
  }

  public setIssueListComp(issueList2Comp: IssuelistComponent) {
    this.issueList2Comp = issueList2Comp;
  }

  public getIssueListComp() {
    return this.issueList2Comp;
  }

  public setIssueListItemComp(issueListItemComp: IssuelistitemComponent) {
    this.issueListItemComp = issueListItemComp;
  }

  public getIssueListItemComp() {
    return this.issueListItemComp;
  }

  public setDrawingsViewComp(drawingsViewComp: DrawingsviewComponent) {
    this.drawingsViewComp = drawingsViewComp;
  }

  public getDrawingsViewComp() {
    return this.drawingsViewComp;
  }

  public getCurrComp() {
    return this.currComp;
  }

  public setCurrComp(company: Company) {
    this.currComp = company;
  }

  public getCurrProj() {
    return this.currProj;
  }

  public setCurrProj(project: Project) {
    this.currProj = project;
  }

  public updateCurrProj() {
    this.project_messageSource.next(this.currProj);
  }

  public updateProjectList() {
    this.projects_messageSource.next(null);
  }

  public getCurrIssue() {
    return this.currIssue;
  }

  public setCurrIssue(issue: Issue) {
    this.currIssue = issue;
  }

  public getCurrDwg() {
    return this.currDwg;
  }

  public setCurrDwg(dwg: Drawing) {
    this.currDwg = dwg;
  }

  public setConsoleDialog(consoleDialog: ConsoledialogComponent) {
    this.consoleDialog = consoleDialog;
  }

  public getConsoleDialog() {
    return this.consoleDialog;
  }

}
