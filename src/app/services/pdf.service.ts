import { AngularFireAnalytics } from '@angular/fire/analytics';
import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { Issue } from 'src/models/issue.model';
import { Photo } from 'src/models/photo.model';
import { ScService } from './sc.service';
import { Drawing } from 'src/models/drawing.model';
import { Dwgmark } from 'src/models/dwgmark.model';
import { Company } from 'src/models/company.model';

declare var PDFDocument: any;
declare var blobStream: any;
declare const fabric: any;

@Injectable({
  providedIn: 'root'
})
export class PdfService {
  issueCnts: IssueCnt[] = [];
  rowCnt: number = 0;
  total_pages: number;
  curr_page: number = 1;
  exhibit_cnt: number = 1;
  dwg: Drawing;
  xOff: number = 0;
  yOff: number = 0;

  private dwgmarkImage: any;
  private doc: any;

  constructor(
    private scService: ScService,
    protected localStorage: LocalStorage,
    public dialog: MatDialog,
    private analytics: AngularFireAnalytics,
  ) { }

  async pdfIt() {
    console.log("pdfIt!");
    this.analytics.logEvent('pdfIt!');
    if (this.scService.getCurrProj() == null) {
      return;
    }
    this.rowCnt = 0;
    this.total_pages = 0;
    this.curr_page = 1;
    this.exhibit_cnt = 0;
    this.doc = new PDFDocument({
      margin: 0,
      autoFirstPage: false,
      layout: "landscape"
    });
    this.doc.info['Title'] = "punch.pdf";
    // 72 units per inch
    // margin should be 29
    await this.calculate();

    await this.doCoverpage();
    await this.doDrawings();
    await this.doIssues();

  }

  async doCoverpage() {
    this.doc.addPage({
      layout: "landscape",
      margin: 0
    });
    this.total_pages = this.total_pages + 1;

    this.doc.fontSize(10);
    this.doc.font('Helvetica-Bold');

    await this.localStorage.getItem<Company>("company").toPromise().then(async (company_) => {
      var comp = company_ as Company;

      if (comp.street1) {
        this.doc.text(comp.street1, 50, 30);
      } else {
        this.doc.text("Street Address.", 50, 30);
      }
      if (comp.city && comp.state && comp.zip) {
        this.doc.text(comp.city + ", " + comp.state + " " + comp.zip, 50, 42);
      } else {
        this.doc.text("City, State ZIP", 50, 42);
      }
      this.doc.text("Telephone", 253, 30);
      if (comp.telephone) {
        this.doc.text(comp.telephone, 253, 42);
      } else {
        this.doc.text("###-###-####", 253, 42);
      }
      this.doc.text("Fax", 460, 30);
      if (comp.fax) {
        this.doc.text(comp.fax);
      } else {
        this.doc.text("###-###-####", 460, 42);
      }
      this.doc.text("Internet Address", 615, 30);
      if (comp.internet) {
        this.doc.text(comp.internet);
      } else {
        this.doc.text("http://www.business.com", 615, 42);
      }

      // this.doc.rect(40, 60, 710, 60)
      //   .lineWidth(2)
      //   .fillOpacity(0.8)
      //   .fillAndStroke("beige", "black");

      if(comp.logo1) {
        this.doc.image(comp.logo1, 40, 60);
      }

      this.doc.fontSize(10);
      this.doc.fillColor("black");
      this.doc.font('Helvetica-Bold');
      this.doc.text("FIELD REPORT",
        355, 130,
        {
          width: 200,
          align: 'CENTER'
        });

      this.doc.fontSize(12);
      if (this.scService.getCurrProj().name) {
        this.doc.text(this.scService.getCurrProj().name,
          353, 155,
          {
            width: 200,
            align: 'CENTER'
          });
      } else {
        this.doc.text("Project Name",
          353, 155,
          {
            width: 200,
            align: 'CENTER'
          });
      }

      if (this.scService.getCurrProj().number) {
        this.doc.text(this.scService.getCurrProj().number,
          348, 170,
          {
            width: 200,
            align: 'CENTER'
          });
      } else {
        this.doc.text("Project Number",
          348, 170,
          {
            width: 200,
            align: 'CENTER'
          });
      }


      this.doc.fontSize(12);
      this.doc.font('Helvetica');

      this.doc.moveTo(120, 200)
        .lineWidth(1)
        .lineTo(640, 200)
        .stroke();

      this.doc.text("The sole purpose of this report is to alert the Contractor that portions of the installations are not in accordance with the contract documents, by either materials or workmanship.",
        120, 205,
        {
          width: 540,
          align: 'left'
        });

      this.doc.rect(120, 239, 525, 61)
        .lineWidth(2)
        .fillOpacity(0.8)
        .fillAndStroke("blue", "black");

      this.doc.fontSize(14);
      this.doc.font('Helvetica-Bold');
      this.doc.fillColor("white").text("The designated Contractor shall respond within seven (7) days indicating that the noted items have been corrected and / or state their position if the item(s) are in dispute.",
        133, 246,
        {
          width: 500,
          align: 'center'
        });

      this.doc.fontSize(12);
      this.doc.font('Helvetica');
      this.doc.fillColor("black").text("Items will be listed in sequence in subsequent reports. All items will stay active until we receive copies of our report with corrected items checked off and initialed by the designated Contractor. The Contractor shall then show/demonstrate to the Field Observer that the item(s) have been corrected. Once verification has been made that the item(s) have been satisfactorily remedied, they will be initialed by the Engineer.",
        120, 309,
        {
          width: 540,
          align: 'left'
        });

      this.doc.text("It should be understood that items not noted that are in violation of the contract does not waive the contractor's obligation to conform to the contract.",
        120, 385,
        {
          width: 540,
          align: 'left'
        });

      this.doc.text("Questions concerning any items should be referred to:",
        120, 430,
        {
          width: 540,
          align: 'left'
        });

      this.doc.text("Company Represantative:",
        120, 460,
        {
          width: 540,
          align: 'left'
        });
      this.doc.text("Copies to:",
        120, 490,
        {
          width: 540,
          align: 'left'
        });
      this.doc.fontSize(10);
      this.doc.font('Helvetica-Bold');
      this.doc.text("Date of Report:",
        550, 510,
        {
          width: 100,
          align: 'left'
        });
      var date = new Date();
      var day = date.getDate();
      var monthIndex = date.getMonth();
      var year = date.getFullYear();
      var minutes = date.getMinutes();
      var hours = date.getHours();
      var seconds = date.getSeconds();
      var myFormattedDate = (monthIndex + 1) + "/" + day + "/" + year + "-" + hours + ":" + minutes + ":" + seconds;

      this.doc.fontSize(8);
      this.doc.font('Helvetica-Bold');
      this.doc.text(myFormattedDate,
        28, 557,
        {
          width: 540,
          align: 'left'
        });

      this.curr_page = 1;
      this.addPageFooter(this.doc);
      this.curr_page = 2;

    });

  }

  async doDrawings() {
    for (var dwgkey of this.scService.getCurrProj().drawingkeys) {
      await this.localStorage.getItem<Drawing>("dwg-" + dwgkey).toPromise().then(async (dwg_) => {
        this.dwg = dwg_ as Drawing;
        this.doc.addPage({
          layout: "landscape",
          margin: 0
        });

        var base64str = this.dwg.dataUrl.replace(/^data:image\/[a-z]+;base64,/, "");
        while (base64str.length % 4 > 0) {
          base64str += '=';
        }
        base64str = "data:image/png;base64," + base64str;

        var width = 750;
        var height = 500;
        this.dwg.scale = Math.min((width / this.dwg.width), (height / this.dwg.height));

        this.xOff = (792 - (this.dwg.width * this.dwg.scale)) / 2;
        this.yOff = (612 - (this.dwg.height * this.dwg.scale)) / 2;

        // window.alert("xOff,yOff = " + this.xOff + "," + this.yOff);

        console.log("xOff,yOff = " + this.xOff + "," + this.yOff);

        this.doc.rect(this.xOff, this.yOff, this.dwg.width * this.dwg.scale, this.dwg.height * this.dwg.scale)
          .lineWidth(4)
          .fillAndStroke("transparent", "black");

        //11 x 8.5 @ 72 dpi / inch = 792 x 612
        // this.scale_ = (792 / dwg.width);

        this.doc.image(base64str, this.xOff, this.yOff, { scale: this.dwg.scale });
        await this.loadImage("assets/map-marker-2-32.png").then(async (img) => {
          console.log("Preload ", img);
          this.dwgmarkImage = img;
          for (var dwgmark of this.dwg.dwgmarks) {
            await this.createImage(dwgmark);
          }
        })
      });
      this.addPageHeader(this.doc);
      this.addPageFooter(this.doc);
      this.curr_page++;
    };
  }

  async doIssues() {

    await this.addPage(this.doc, true);

    this.rowCnt = 0;
    if (this.issueCnts != null && this.issueCnts.length > 0) {
      for (var i = 0; i < this.issueCnts.length; i++) {
        if (this.rowCnt > 0 && this.rowCnt % 12 == 0) {
          this.curr_page++;
          await this.addPage(this.doc, true);
          this.rowCnt = 0;
        }
        await this.doIssueRow(this.doc, this.issueCnts[i], this.rowCnt);
        this.rowCnt++;
      }
    }

    var exhibit_no = 1;
    var img_cnt_page = 1;
    var first_page_added = false;
    for (var i = 0; i < this.scService.getCurrProj().issues.length; i++) {
      var issue = this.scService.getCurrProj().issues[i];
      if (issue.photokeys != null) {
        for (var j = 0; j < issue.photokeys.length; j++) {
          await this.localStorage.getItem<Photo>("photo-" + issue.photokeys[j]).toPromise().then(async <Photo>(photo) => {
            console.log("photo exists! img_cnt_page == " + img_cnt_page);
            if (photo != null) {
              console.log("photo!=null " + photo.dataUrl.length);
              if (!first_page_added) {
                this.curr_page++;
                await this.addPage(this.doc, false);
                first_page_added = true;
              } else if (img_cnt_page > 4) {
                this.curr_page++;
                await this.addPage(this.doc, false);
                img_cnt_page = 1;
              }
              var base64str = photo.dataUrl.replace(/^data:image\/[a-z]+;base64,/, "");
              while (base64str.length % 4 > 0) {
                base64str += '=';
              }
              base64str = "data:image/png;base64," + base64str;

              var w = 320;
              var h = 240;
              var scale = Math.min((w / photo.width), (h / photo.height));

              var x1, y1: number;
              var width = (photo.width * scale);
              var height = (photo.height * scale);

              if (img_cnt_page == 1) {
                x1 = (320 - width) / 2 + 60;
                y1 = (240 - height) / 2 + 52;
                console.log("width = " + width);
                console.log("x1 = " + x1);
                this.doc.image(base64str, x1, y1, { fit: [320, 240] });
                this.doc.moveTo(x1, y1)
                  .lineTo(x1 + width, y1)
                  .lineTo(x1 + width, y1 + height)
                  .lineTo(x1, y1 + height)
                  .lineTo(x1, (y1 - 1))
                  .lineWidth(2)
                  .stroke();
                var exhibit_txt = "Exhibit #" + exhibit_no;
                this.doc.fontSize(11);
                var string_width = this.doc.widthOfString(exhibit_txt);
                this.doc.text(exhibit_txt, 220 - (string_width / 2), 297);
              } else if (img_cnt_page == 2) {
                x1 = (320 - width) / 2 + 411;
                y1 = (240 - height) / 2 + 52;
                this.doc.image(base64str, x1, y1, { fit: [320, 240] });
                this.doc.moveTo(x1, y1)
                  .lineTo(x1 + width, y1)
                  .lineTo(x1 + width, y1 + height)
                  .lineTo(x1, y1 + height)
                  .lineTo(x1, (y1 - 1))
                  .lineWidth(2)
                  .stroke();
                var exhibit_txt = "Exhibit #" + exhibit_no;
                this.doc.fontSize(11);
                var string_width = this.doc.widthOfString(exhibit_txt);
                this.doc.text(exhibit_txt, 571 - (string_width / 2), 297);
              } else if (img_cnt_page == 3) {
                x1 = (320 - width) / 2 + 60;
                y1 = (240 - height) / 2 + 311;
                this.doc.image(base64str, x1, y1, { fit: [320, 240] });
                this.doc.moveTo(x1, y1)
                  .lineTo(x1 + width, y1)
                  .lineTo(x1 + width, y1 + height)
                  .lineTo(x1, y1 + height)
                  .lineTo(x1, (y1 - 1))
                  .lineWidth(2)
                  .stroke();
                var exhibit_txt = "Exhibit #" + exhibit_no;
                this.doc.fontSize(11);
                var string_width = this.doc.widthOfString(exhibit_txt);
                this.doc.text(exhibit_txt, 220 - (string_width / 2), 556);
              } else if (img_cnt_page == 4) {
                x1 = (320 - width) / 2 + 411;
                y1 = (240 - height) / 2 + 311;
                this.doc.image(base64str, x1, y1, { fit: [320, 240] });
                this.doc.moveTo(x1, y1)
                  .lineTo(x1 + width, y1)
                  .lineTo(x1 + width, y1 + height)
                  .lineTo(x1, y1 + height)
                  .lineTo(x1, (y1 - 1))
                  .lineWidth(2)
                  .stroke();
                var exhibit_txt = "Exhibit #" + exhibit_no;
                this.doc.fontSize(11);
                var string_width = this.doc.widthOfString(exhibit_txt);
                this.doc.text(exhibit_txt, 571 - (string_width / 2), 556);
              }
              img_cnt_page++;
              exhibit_no++;
            }
          });
        }
      }
    }
    this.close();

  }

  private async addPage(doc: any, table: boolean) {
    doc.addPage({
      layout: "landscape",
      margin: 0
    });
    // doc.moveTo(29, 55)
    //   .lineTo(763, 55)
    //   .lineTo(763, 560)
    //   .lineTo(29, 560)
    //   .lineTo(29, 55)
    //   .stroke();

    await this.addPageHeader(doc);
    if (table) {
      await this.addTableHeader(doc);
    }
    await this.addPageFooter(doc);

  }

  private async addPageHeader(doc: any) {
    var project_name: string = "Project Name";
    if (this.scService.getCurrProj().name != null) {
      project_name = this.scService.getCurrProj().name;
    }
    var project_no: string = "Project No.";
    if (this.scService.getCurrProj().number != null) {
      project_no = this.scService.getCurrProj().number;
    }
    var report_title: string = "MEP/FP Field Report";
    if (this.scService.getCurrProj().top_right_text != null) {
      report_title = this.scService.getCurrProj().top_right_text;
    }

    doc.fontSize(10);
    var string_height = doc.heightOfString("Project: " + project_name, { width: 1000 });

    doc.fontSize(8);
    var string_width = doc.widthOfString(report_title);

    doc.text("Project: " + project_name, 29, 29);
    doc.text("Job# " + project_no, 29, (29 + string_height));
    doc.text(report_title, 763 - (string_width), 29);

  }

  private async addTableHeader(doc: any) {
    var col1_width: number = 45;
    var col2_width: number = 60;
    var col3_width: number = 60;
    var col4_width: number = 150;
    var col5_width: number = 299;
    var col6_width: number = 60;
    var col7_width: number = 60;
    var row_height: number = 30;

    doc.lineWidth(3);

    var _x = 29;
    var _y = 55;
    doc.rect(_x, _y, col1_width, row_height)
      .lineWidth(1)
      .fillAndStroke("#D3D3D3", "black");

    doc.fontSize(10);
    doc.fillColor("black");
    var item_text = "Item"
    var text_width = doc.widthOfString(item_text);
    var text_height = doc.heightOfString(item_text);
    doc.text(item_text, _x + (col1_width / 2) - (text_width / 2), _y + ((row_height / 2) - (text_height / 2)) + 2);

    var _x = 29 + col1_width;
    doc.rect(_x, _y, col2_width, row_height)
      .lineWidth(1)
      .fillAndStroke("#D3D3D3", "black");

    doc.fontSize(10);
    doc.fillColor("black");
    item_text = "Trade"
    text_width = doc.widthOfString(item_text);
    text_height = doc.heightOfString(item_text);
    doc.text(item_text, _x + (col2_width / 2) - (text_width / 2), _y + ((row_height / 2) - (text_height / 2)) + 2);

    var _x = 29 + col1_width + col2_width;
    doc.rect(_x, _y, col3_width, row_height)
      .lineWidth(1)
      .fillAndStroke("#D3D3D3", "black");

    doc.fontSize(10);
    doc.fillColor("black");
    item_text = "Floor"
    text_width = doc.widthOfString(item_text);
    text_height = doc.heightOfString(item_text);
    doc.text(item_text, _x + (col3_width / 2) - (text_width / 2), _y + ((row_height / 2) - (text_height / 2)) + 2);

    var _x = 29 + col1_width + col2_width + col3_width;
    doc.rect(_x, _y, col4_width, row_height)
      .lineWidth(1)
      .fillAndStroke("#D3D3D3", "black");

    doc.fontSize(10);
    doc.fillColor("black");
    item_text = "Location"
    text_width = doc.widthOfString(item_text);
    text_height = doc.heightOfString(item_text);
    doc.text(item_text, _x + (col4_width / 2) - (text_width / 2), _y + ((row_height / 2) - (text_height / 2)) + 2);

    var _x = 29 + col1_width + col2_width + col3_width + col4_width;
    doc.rect(_x, _y, col5_width, row_height)
      .lineWidth(1)
      .fillAndStroke("#D3D3D3", "black");

    doc.fontSize(10);
    doc.fillColor("black");
    item_text = "Description"
    text_width = doc.widthOfString(item_text);
    text_height = doc.heightOfString(item_text);
    doc.text(item_text, _x + (col5_width / 2) - (text_width / 2), _y + ((row_height / 2) - (text_height / 2)) + 2);

    var _x = 29 + col1_width + col2_width + col3_width + col4_width + col5_width;
    doc.rect(_x, _y, col6_width, row_height)
      .lineWidth(1)
      .fillAndStroke("#D3D3D3", "black");

    doc.fontSize(10);
    doc.fillColor("black");
    item_text = "Date of Visit"
    text_width = doc.widthOfString(item_text);
    text_height = doc.heightOfString(item_text);
    doc.text(item_text, _x + (col6_width / 2) - (text_width / 2), _y + ((row_height / 2) - (text_height / 2)) + 2);

    var _x = 29 + col1_width + col2_width + col3_width + col4_width + col5_width + col6_width;
    doc.rect(_x, _y, col7_width, row_height)
      .lineWidth(1)
      .fillAndStroke("#D3D3D3", "black");

    doc.fontSize(10);
    doc.fillColor("black");
    item_text = "Exhibit #"
    text_width = doc.widthOfString(item_text);
    text_height = doc.heightOfString(item_text);
    doc.text(item_text, _x + (col7_width / 2) - (text_width / 2), _y + ((row_height / 2) - (text_height / 2)) + 2);

  }

  private async addPageFooter(doc: any) {
    var footer_left1: string = "M-Mechanical   E-Electrical";
    if (this.scService.getCurrProj().bot_left_text_1 != null) {
      footer_left1 = this.scService.getCurrProj().bot_left_text_1;
    }
    var footer_left2: string = "P-Plumbing   FP-Fire Protection";
    if (this.scService.getCurrProj().bot_left_text_2 != null) {
      footer_left2 = this.scService.getCurrProj().bot_left_text_2;
    }

    var string_height = doc.heightOfString(footer_left1, { width: 1000 });

    doc.fillColor("black");
    doc.strokeColor("black");

    doc.fontSize(8);
    doc.text(footer_left1, 29, 567);
    doc.text(footer_left2, 29, doc.y);

    var my_date: string = new DatePipe("en-US").transform(new Date().getTime(), 'MM/dd/yyyy');
    if (this.scService.getCurrProj().report_date != null) {
      my_date = this.scService.getCurrProj().report_date;
    }
    var page_no: string = "Page " + this.curr_page + " of " + this.total_pages;

    var string_width = doc.widthOfString(my_date);
    doc.text(my_date, 763 - (string_width), 567);

    string_width = doc.widthOfString(page_no);
    doc.text(page_no, 763 - (string_width), doc.y);

  }

  private async doIssueRow(doc: any, issueCnt: IssueCnt, row_cnt: number) {
    var col1_width: number = 45;
    var col2_width: number = 60;
    var col3_width: number = 60;
    var col4_width: number = 150;
    var col5_width: number = 299;
    var col6_width: number = 60;
    var col7_width: number = 60;
    var row_height: number = 39.6;

    doc.lineWidth(3);

    var _x = 29;
    var _y = 85 + (row_height * row_cnt);
    doc.rect(_x, _y, col1_width, row_height)
      .lineWidth(1)
      .fillAndStroke("white", "black");

    doc.fontSize(10);
    doc.fillColor("black");
    var item_text = issueCnt.issue.no + "";
    var text_width = doc.widthOfString(item_text);
    var text_height = doc.heightOfString(item_text);
    doc.text(item_text, _x + (col1_width / 2) - (text_width / 2), _y + ((row_height / 2) - (text_height / 2)) + 2);

    var _x = 29 + col1_width;
    doc.rect(_x, _y, col2_width, row_height)
      .lineWidth(1)
      .fillAndStroke("white", "black");

    if (issueCnt.issue.trade != null) {
      doc.fontSize(10);
      doc.fillColor("black");
      item_text = issueCnt.issue.trade;
      text_width = doc.widthOfString(item_text, { width: col2_width - 5 });
      text_height = doc.heightOfString(item_text, { width: col2_width - 5 });
      doc.text(item_text, (_x + 2), (_y + ((row_height / 2) - (text_height / 2)) + 2),
        {
          width: (col2_width - 5),
          align: 'center'
        });
    }

    var _x = 29 + col1_width + col2_width;
    doc.rect(_x, _y, col3_width, row_height)
      .lineWidth(1)
      .fillAndStroke("white", "black");

    doc.fontSize(10);
    doc.fillColor("black");
    if (issueCnt.issue.floor != null) {
      item_text = issueCnt.issue.floor;
      text_width = doc.widthOfString(item_text, { width: col3_width - 5 });
      text_height = doc.heightOfString(item_text, { width: col3_width - 5 });
      doc.text(item_text, (_x + 2), (_y + ((row_height / 2) - (text_height / 2)) + 2),
        {
          width: (col3_width - 5),
          align: 'center'
        });
    }

    var _x = 29 + col1_width + col2_width + col3_width;
    doc.rect(_x, _y, col4_width, row_height)
      .lineWidth(1)
      .fillAndStroke("white", "black");

    if (issueCnt.issue.location != null) {
      doc.fontSize(10);
      doc.fillColor("black");
      item_text = issueCnt.issue.location;
      text_width = doc.widthOfString(item_text, { width: col4_width - 5 });
      text_height = doc.heightOfString(item_text, { width: col4_width - 5 });
      doc.text(item_text, _x + 2, (_y + ((row_height / 2) - (text_height / 2)) + 2),
        {
          width: (col4_width - 5),
          align: 'center'
        });
    }

    var _x = 29 + col1_width + col2_width + col3_width + col4_width;
    doc.rect(_x, _y, col5_width, row_height)
      .lineWidth(1)
      .fillAndStroke("white", "black");

    if (issueCnt.issue.description != null) {
      doc.fontSize(10);
      doc.fillColor("black");
      item_text = issueCnt.issue.description;
      text_width = doc.widthOfString(item_text);
      text_height = doc.heightOfString(item_text, { width: col5_width - 5 });
      console.log("row height = " + row_height + ", text_height = " + text_height);
      doc.text(item_text, (_x + 2), (_y + ((row_height / 2) - (text_height / 2)) + 2),
        {
          width: (col5_width - 5),
          align: 'left'
        });
    }

    var _x = 29 + col1_width + col2_width + col3_width + col4_width + col5_width;
    doc.rect(_x, _y, col6_width, row_height)
      .lineWidth(1)
      .fillAndStroke("white", "black");

    if (issueCnt.issue.open_date != null) {
      doc.fontSize(10);
      doc.fillColor("black");
      item_text = new DatePipe("en-US").transform(new Date(issueCnt.issue.open_date), 'MM/dd/yyyy');
      text_width = doc.widthOfString(item_text);
      text_height = doc.heightOfString(item_text);
      doc.text(item_text, _x + (col6_width / 2) - (text_width / 2), _y + ((row_height / 2) - (text_height / 2)) + 2);
    }

    var _x = 29 + col1_width + col2_width + col3_width + col4_width + col5_width + col6_width;
    doc.rect(_x, _y, col7_width, row_height)
      .lineWidth(1)
      .fillAndStroke("white", "black");

    if (issueCnt.exhibits != null) {
      doc.fontSize(10);
      doc.fillColor("black");
      item_text = issueCnt.exhibits;
      text_width = doc.widthOfString(item_text, { width: col7_width - 5 });
      text_height = doc.heightOfString(item_text, { width: col7_width - 5 });
      doc.text(item_text, (_x + 2), (_y + ((row_height / 2) - (text_height / 2)) + 2),
        {
          width: (col7_width - 5),
          align: 'center'
        });
    }
  }

  private calculate() {
    this.total_pages = this.scService.getCurrProj().drawingkeys.length;
    this.issueCnts.length = 0;
    if (this.scService.getCurrProj() == null) {
      console.error("Current Project == null!");
      return;
    }
    var no_issues = this.scService.getCurrProj().issues.length;
    this.total_pages = this.total_pages + Math.ceil(no_issues / 12);
    // console.log("There are " + no_issues + " issues, resulting in " + this.total_pages + " total issue pages!");
    for (var i = 0; i < this.scService.getCurrProj().issues.length; i++) {
      var issue = this.scService.getCurrProj().issues[i];
      var issueCnt = new IssueCnt();
      issueCnt.issue = issue;
      if (issue.photokeys != null && issue.photokeys.length > 0) {
        for (var j = 0; j < issue.photokeys.length; j++) {
          if (j == 0) {
            issueCnt.exhibits = this.exhibit_cnt + 1 + "";
          } else {
            issueCnt.exhibits = issueCnt.exhibits + "," + (this.exhibit_cnt + 1);
          }
          this.exhibit_cnt++;
        }
      }
      this.issueCnts.push(issueCnt);
    }
    this.total_pages = this.total_pages + Math.ceil(this.exhibit_cnt / 4);
    // console.log("There are a grand total of " + this.total_pages + " total pages!");
  }

  createImage(dwgmark: Dwgmark) {   // load an image function 
    // creates a new i each time it is called
    var canvas = new fabric.Canvas('thecanvas');
    canvas.width = 128;
    canvas.height = 128;
    var imageInstance = new fabric.Image(this.dwgmarkImage, {
      left: 0,
      top: 0,
      angle: 0,
      opacity: 1,
      // width: (64),
      // height: (64),
    });
    canvas.add(imageInstance);
    var dataURL = canvas.toDataURL("image/png");
    // console.log("dataURL=" + dataURL);
    var x_ = this.xOff+(dwgmark.x*this.dwg.width*this.dwg.scale)-28;
    var y_ = this.yOff+(dwgmark.y*this.dwg.height*this.dwg.scale)-32;
    console.log("x_,y_=" + x_ + "," + y_);
    this.doc.fontSize(14);
    var string_width = this.doc.widthOfString("#" + dwgmark.issue_no);
    this.doc.image(dataURL, (x_+string_width), y_, { scale: 1 });
    this.doc.text("#" + dwgmark.issue_no, x_, y_, { width: string_width, align: 'right' });
    // this.doc.circle((x_), (y_), 8).lineWidth(2).stroke("yellow");
  }

  loadImage(src: string) {
    return new Promise((resolve, reject) => {
      console.log(src);
      var img = new Image();
      img.onload = () => resolve(img);
      img.src = src;
    });
  }

  close() {
    var stream = this.doc.pipe(blobStream())
    this.doc.end();
    stream.on("finish", () => {
      var url = stream.toBlobURL('application/pdf');
      var blob = stream.toBlob('application/pdf');
      this.scService.getIssueListComp().setTimer(false);

      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        console.log("here");
        window.navigator.msSaveOrOpenBlob(blob, this.scService.getCurrProj().name + ".pdf");
      }
      else {
        var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
        if(iOS) {
          window.open(url, "fieldreport.pdf");
        } else {
          console.log("here1");
          window.open(url, "fieldreport.pdf");
        }
      }
      // const dialogRef = this.dialog.open(PdfdialogComponent, {
      //   data: { url: url },
      //   panelClass: 'app-full-bleed-dialog2',
      //   width: '100vw',
      //   maxWidth: '100vw',
      //   height: '100vh'
      // })
      // dialogRef.afterClosed().subscribe(result => {
      //   console.log("dialog closed!");
      // });

    });
  }
}

export class IssueCnt {
  issue: Issue;
  exhibits: string;

  constructor() {
  }

}