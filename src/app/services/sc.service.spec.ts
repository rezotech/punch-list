import { TestBed } from '@angular/core/testing';

import { ScService } from './sc.service';

describe('ScService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScService = TestBed.get(ScService);
    expect(service).toBeTruthy();
  });
});
