import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotograbComponent } from './photograb.component';

describe('PhotograbComponent', () => {
  let component: PhotograbComponent;
  let fixture: ComponentFixture<PhotograbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotograbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotograbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
