import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ScService } from '../services/sc.service';
import { Photo } from 'src/models/photo.model';


@Component({
  selector: 'app-photograb',
  templateUrl: './photograb.component.html',
  styleUrls: ['./photograb.component.css']
})
export class PhotograbComponent implements OnInit {
  private open: boolean = true;
  public captures: Array<any>;
  public camWidth = window.innerWidth * .9;
  public camHeight = window.innerHeight * .45;
  public imgWidth = window.innerWidth * .35;
  public imgHeight = 0;
  private photoKeys: string[] = [];

  @ViewChild("video", { read: ElementRef, static: true  })
  public video: ElementRef;

  @ViewChild("oc", { read: ElementRef, static: true  })
  public ocbutton: ElementRef;

  @ViewChild("canvas", { read: ElementRef, static: true  })
  public canvas: ElementRef;

  mediaStream: MediaStream;

  constructor(
    protected localStorage: LocalStorage,
    private scService: ScService
  ) {
    this.captures = [];
  }

  ngOnInit() {
    this.scService.setPhotograbComp(this);
    console.log("Here!");
    if (this.scService.getCurrIssue().photokeys != null && this.scService.getCurrIssue().photokeys.length > 0) {
      console.log("Here 2!");
      this.captures = [];
      for (var i = 0; i < this.scService.getCurrIssue().photokeys.length; i++) {
        this.localStorage.getItem<Photo>("photo-" + this.scService.getCurrIssue().photokeys[i]).subscribe((photo_) => {
          var photo = photo_ as Photo;
          var scale = this.imgWidth/photo.width;

          var iwScaled = scale*photo.width;
          var ihScaled = scale*photo.height;
          var width = (window.innerWidth-40)/2;
          var height = (window.innerHeight-40)/2;
          var scale = Math.min(width / photo.width, height / photo.height);
          console.log("img.width, window.width = " + photo.width + "," + window.innerWidth);
          console.log("scale=" + scale);
          photo.width = photo.width * scale;
          photo.height = photo.height * scale;
          this.imgHeight = photo.height;
          this.captures.push(photo);
          console.log("captures.length = " + this.captures.length);
        }, (error) => {
          console.log("Error getting photo!");
        });
        this.scService.getAppComp().showSpinner(false);
      }
    }
  }

  public ngAfterViewInit() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      const velem: HTMLVideoElement = this.video.nativeElement;
      navigator.mediaDevices.getUserMedia({ audio: false, video: { facingMode: "environment" } }).then(stream => {
        this.mediaStream = stream;
        velem.srcObject = this.mediaStream;
        velem.play();
      });
      let supoorted = navigator.mediaDevices.getSupportedConstraints();
    }
    console.log("screen dimensions : " + window.innerWidth + ", " + window.innerHeight);
  }

  public stop() {
    try {
      this.mediaStream.getTracks().forEach(track => {
        track.stop();
      });
    } catch {
      console.log("mediaStream fail!");
    }
   
  }

  public capture() {
    const velem: HTMLVideoElement = this.video.nativeElement;
    console.log("w,h = " + velem.videoWidth + ", " + velem.videoHeight);
    //var scale = Math.min((width / velem.videoWidth), (height / velem.videoHeight));
    var scale = this.imgWidth/velem.videoWidth;

    var iwScaled = scale*velem.videoWidth;
    var ihScaled = scale*velem.videoHeight;
    this.imgHeight = ihScaled;

    var canvas = document.createElement('canvas');
    canvas.width = iwScaled;
    canvas.height = ihScaled;
    // var context2 = canvas.getContext('2d');
    // var img: HTMLImageElement = new Image();

    var canvas = document.createElement('canvas');
    canvas.width = iwScaled;
    canvas.height = ihScaled;
    var context = canvas.getContext("2d").drawImage(this.video.nativeElement, 0, 0, iwScaled, ihScaled);
    var imgqual = .5
    var dataUrl = canvas.toDataURL("image/jpeg", imgqual);

    var photoKey = new Date().getTime() + "";
    var photo: Photo = new Photo();
    photo.id = photoKey;
    photo.dataUrl = dataUrl;
    photo.width = velem.videoWidth;
    photo.height = velem.videoHeight;
    photo.scale = scale;
    this.scService.getCurrIssue().photokeys.push(photoKey);
    this.localStorage.setItem(this.scService.getCurrProj().key, this.scService.getCurrProj()).subscribe(() => {
      console.log("project updated!")
    });
    this.localStorage.setItem("photo-" + photoKey, photo).subscribe(() => {
      console.log("photo saved!");
    });
    // this.scService.getIssueListItemComp().update();

    // var photoUrl = URL.createObjectURL(this.convertDataUrlToBlob(dataUrl));
    // img.onload = (event) => {
    //   console.log("img onload");
    //   context2.drawImage(img, 0, 0, iwScaled, ihScaled);
    //   var dataUrl = canvas.toDataURL("image/png");
    //   console.log("dataUrl = " + dataUrl);
    //   var photoKey = new Date().getTime() + "";
    //   var photo: Photo = new Photo();
    //   photo.id = photoKey;
    //   photo.dataUrl = dataUrl;
    //   photo.width = velem.videoWidth;
    //   photo.height = velem.videoHeight;
    // };
    // img.src = dataUrl;
    // console.log("img.src = " + dataUrl);
    this.captures.push(photo);

  }

  public openclose() {
    const velem: HTMLVideoElement = this.video.nativeElement;
    if (!this.open) {
      velem.play();
      this.ocbutton.nativeElement.innerHTML = "Pause";
      this.open = true;
    } else {
      velem.pause();
      this.ocbutton.nativeElement.innerHTML = "Play";
      this.open = false;
    }
  }

  convertDataUrlToBlob(dataUrl): Blob {
    const arr = dataUrl.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new Blob([u8arr], { type: mime });
  }

}
