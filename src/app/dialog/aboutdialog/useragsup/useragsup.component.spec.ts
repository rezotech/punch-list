import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UseragsupComponent } from './useragsup.component';

describe('UseragsupComponent', () => {
  let component: UseragsupComponent;
  let fixture: ComponentFixture<UseragsupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UseragsupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UseragsupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
