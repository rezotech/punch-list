import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ScService } from 'src/app/services/sc.service';
import { Project } from 'src/models/project.model';
import { AngularFireAnalytics } from '@angular/fire/analytics';

@Component({
  selector: 'app-useragsup',
  templateUrl: './useragsup.component.html',
  styleUrls: ['./useragsup.component.css']
})

export class UseragsupComponent implements OnInit {
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<UseragsupComponent>,
    protected localStorage: LocalStorage,
    private scService: ScService,
    private analytics: AngularFireAnalytics,
  ) {
    
  }

  ngOnInit() {
  }

  cancel() {
    this.dialogRef.close();
  }

  suppress() {
    if(this.scService.getProjectList().aboutdiag) {
      this.scService.getProjectList().aboutdiag = false;
      this.analytics.logEvent('about dialog supressed!');
    } else {
      this.scService.getProjectList().aboutdiag = true;
      this.analytics.logEvent('about dialog un-supressed!');
    }
    this.localStorage.setItem('plist', this.scService.getProjectList()).subscribe(() => {
      console.log("plist updated!");
      this.dialogRef.close();
    });
  }

}
