import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { ScService } from 'src/app/services/sc.service';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { AngularFireAnalytics } from '@angular/fire/analytics';
import { UseragdialogComponent } from './useragdialog/useragdialog.component';
import { UseragsupComponent } from './useragsup/useragsup.component';

@Component({
  selector: 'app-aboutdialog',
  templateUrl: './aboutdialog.component.html',
  styleUrls: ['./aboutdialog.component.css']
})
export class AboutdialogComponent implements OnInit {
  @ViewChild("aboutchk", { read: ElementRef, static: true  }) aboutChkEl: ElementRef;
  checked: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<AboutdialogComponent>,
    private scService: ScService,
    protected localStorage: LocalStorage,
    private analytics: AngularFireAnalytics,
    public dialog: MatDialog,
  ) { }

  scheck(checked: boolean) {
    console.log("scheck()");
    // const dialogRef = this.dialog.open(UseragsupComponent, {
    //   panelClass: 'app-full-bleed-dialog',
    //   width: '400px'
    // })
    // dialogRef.afterClosed().subscribe(result => {
    //   console.log("dialog closed!");
    // });
    if(this.scService.getProjectList().aboutdiag) {
      this.scService.getProjectList().aboutdiag = false;
      this.analytics.logEvent('about dialog supressed!');
    } else {
      this.scService.getProjectList().aboutdiag = true;
      this.analytics.logEvent('about dialog un-supressed!');
    }
    this.localStorage.setItem('plist', this.scService.getProjectList()).subscribe(() => {
      console.log("plist updated!");
      this.dialogRef.close();
    });
  }

  googleGroup() {
    window.open("https://groups.google.com/forum/#!forum/fieldreportapp");
  }

  youTube() {
    
  }

  userAgreement() {
    const dialogRef = this.dialog.open(UseragdialogComponent, {
      panelClass: 'app-full-bleed-dialog2',
      width: '80vw',
      height: '80vh'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
    });
    this.analytics.logEvent("User Agreement Opened");
    this.close();
  }

  close() {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.checked = this.scService.getProjectList().aboutdiag;
  }

}
