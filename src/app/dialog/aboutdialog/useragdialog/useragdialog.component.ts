import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { ScService } from 'src/app/services/sc.service';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { AngularFireAnalytics } from '@angular/fire/analytics';

@Component({
  selector: 'app-useragdialog',
  templateUrl: './useragdialog.component.html',
  styleUrls: ['./useragdialog.component.css']
})
export class UseragdialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<UseragdialogComponent>,
    private scService: ScService,
    protected localStorage: LocalStorage,
    private analytics: AngularFireAnalytics,
  ) { }

  
  close() {
    this.dialogRef.close();
    this.analytics.logEvent('userag dialog closed!');
  }

  ngOnInit() {
  }

}
