import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UseragdialogComponent } from './useragdialog.component';

describe('UseragdialogComponent', () => {
  let component: UseragdialogComponent;
  let fixture: ComponentFixture<UseragdialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UseragdialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UseragdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
