import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfdialogComponent } from './pdfdialog.component';

describe('PdfdialogComponent', () => {
  let component: PdfdialogComponent;
  let fixture: ComponentFixture<PdfdialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdfdialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
