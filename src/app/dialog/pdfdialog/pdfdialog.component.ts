import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ScService } from 'src/app/services/sc.service';
import { LocalStorage } from '@ngx-pwa/local-storage';


@Component({
  selector: 'app-pdfdialog',
  templateUrl: './pdfdialog.component.html',
  styleUrls: ['./pdfdialog.component.css']
})
export class PdfdialogComponent implements OnInit {
  // @ViewChild("pdfViewer") pdfViewer;
  @ViewChild("canvasEl", { read: ElementRef, static: true  }) canvasEl: ElementRef; 
  checked: boolean = false;
  url: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<PdfdialogComponent>,
    private scService: ScService,
    protected localStorage: LocalStorage,
  ) {
    this.url = this.data.url;
    
    // PDFJS.getDocument(this.url).then(pdf =>{
    //   pdf.getPage(1).then(page =>{
    //     var scale = 1.5;
    //     var viewport = page.getViewport(scale);

    //     var context = this.canvasEl.nativeElement.getContext('2d');
    //     this.canvasEl.nativeElement.height = viewport.height;
    //     this.canvasEl.nativeElement.width = viewport.width;

    //     var renderContext = {
    //       canvasContext: context,
    //       viewport: viewport
    //     };
    //     page.render(renderContext);
    //   });
    // });
  }

  close() {
    // window.alert("close()! msg=" + this.pdfViewer);
    // window.alert("PDFJS maxCanvasPixels = " + pdfjs.maxCanvasPixels);
    
    this.dialogRef.close();
  }

  ngOnInit() {
  }

  click() {
    window.alert("Click!");
  }

}
