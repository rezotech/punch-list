import { Component, OnInit, Input, Inject, ViewChild, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ScService } from 'src/app/services/sc.service';
import { Company } from 'src/models/company.model';


@Component({
  selector: 'app-compdialog',
  templateUrl: './compdialog.component.html',
  styleUrls: ['./compdialog.component.css']
})
export class CompdialogComponent implements OnInit {
  @ViewChild("fileinput", { read: ElementRef, static: true }) fileinputEl: ElementRef;
  company: Company = new Company();
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<CompdialogComponent>,
    protected localStorage: LocalStorage,
    private scService: ScService,
  ) {
    this.company = this.data.company;
   }

  ngOnInit() {
  }

  cnameBlur(event: any) {
    this.company.name = event.target.value;
    this.localStorage.setItem('company', this.company).subscribe(() => {
      console.log("company name updated!");
    });
  }

  s1Blur(event: any) {
    this.company.street1 = event.target.value;
    this.localStorage.setItem('company', this.company).subscribe(() => {
      console.log("street 1 updated!");
    });
  }

  s2Blur(event: any) {
    this.company.street2 = event.target.value;
    this.localStorage.setItem('company', this.company).subscribe(() => {
      console.log("street 2 updated!");
    });
  }

  cityBlur(event: any) {
    this.company.city = event.target.value;
    this.localStorage.setItem('company', this.company).subscribe(() => {
      console.log("city updated!");
    });
  }

  stateBlur(event: any) {
    this.company.state = event.target.value;
    this.localStorage.setItem('company', this.company).subscribe(() => {
      console.log("state updated!");
    });
  }

  zipBlur(event: any) {
    this.company.zip = event.target.value;
    this.localStorage.setItem('company', this.company).subscribe(() => {
      console.log("zip code updated!");
    });
  }

  teleBlur(event: any) {
    this.company.telephone = event.target.value;
    this.localStorage.setItem('company', this.company).subscribe(() => {
      console.log("telephone updated!");
    });
  }

  faxBlur(event: any) {
    this.company.fax = event.target.value;
    this.localStorage.setItem('company', this.company).subscribe(() => {
      console.log("fax# updated!");
    });
  }

  intBlur(event: any) {
    this.company.internet = event.target.value;
    this.localStorage.setItem('company', this.company).subscribe(() => {
      console.log("internet address updated!");
    });
  }

  async addLogo(e: any) {
    await this.getOrientation(e.target.files[0], async (orientation) => {
      // window.alert("orientation = " + orientation);
      if (orientation >= 2 && orientation <= 8) {
        var fr = new FileReader;
        fr.onload = (async () => {
          var dataUrl = fr.result.toString();
          await this.resetOrientation(dataUrl, orientation, async (resetBase64Image) => {
            this.addPhoto2a(resetBase64Image);
          });
        });
        fr.readAsDataURL(e.target.files[0]);
      } else {
        var fr = new FileReader;
        fr.onload = (() => {
          var dataUrl = fr.result.toString();
          this.addPhoto2a(dataUrl);
        });
        fr.readAsDataURL(e.target.files[0]);
      }
    });
  }

  async addPhoto2a(dataUrl) {
    var img = new Image;

    img.src = dataUrl;
    img.onload = (async () => {

      // window.alert(img.width + "," + img.height); // image is loaded; sizes are available
      // 8.5 x 11 @ 72 dpi = 612 x 792, so make it 572 x 752
      // var width = 600;
      // var height = 400;
      // var scale: number = Math.min((width / img.width), (height / img.height));

      var canvas = document.createElement('canvas');
      canvas.width = img.width;
      canvas.height = img.height;
      console.log("image width,height=" + img.width + ", " + img.height);
      console.log("canvas width,height=" + canvas.width + ", " + canvas.height);
      // console.log("scale = " + scale);
      canvas.getContext("2d").drawImage(img, 0, 0, canvas.width, canvas.height);
      var imgqual = this.scService.getCurrProj().img_qual;
      if (imgqual == null || imgqual < .5) {
        imgqual = .5;
      }
      this.company.logo1 = canvas.toDataURL("image/jpg", imgqual);

      this.localStorage.setItem("company", this.company).subscribe(() => {
        console.log("logo image updated.")
        this.fileinputEl.nativeElement.value = "";
      });
    });
  }

  getOrientation = (file: File, callback: Function) => {
    var reader = new FileReader();

    reader.onload = (event: ProgressEvent) => {

      if (!event.target) {
        return;
      }

      const file = event.target as FileReader;
      const view = new DataView(file.result as ArrayBuffer);

      if (view.getUint16(0, false) != 0xFFD8) {
        return callback(-2);
      }

      const length = view.byteLength
      let offset = 2;

      while (offset < length) {
        if (view.getUint16(offset + 2, false) <= 8) return callback(-1);
        let marker = view.getUint16(offset, false);
        offset += 2;

        if (marker == 0xFFE1) {
          if (view.getUint32(offset += 2, false) != 0x45786966) {
            return callback(-1);
          }

          let little = view.getUint16(offset += 6, false) == 0x4949;
          offset += view.getUint32(offset + 4, little);
          let tags = view.getUint16(offset, little);
          offset += 2;
          for (let i = 0; i < tags; i++) {
            if (view.getUint16(offset + (i * 12), little) == 0x0112) {
              return callback(view.getUint16(offset + (i * 12) + 8, little));
            }
          }
        } else if ((marker & 0xFF00) != 0xFF00) {
          break;
        }
        else {
          offset += view.getUint16(offset, false);
        }
      }
      return callback(-1);
    };
    reader.readAsArrayBuffer(file);
  }

  async resetOrientation(srcBase64, srcOrientation, callback) {
    var img = new Image();

    img.onload = (async () => {
      var width = img.width,
        height = img.height,
        canvas = document.createElement('canvas'),
        ctx = canvas.getContext("2d");

      // set proper canvas dimensions before transform & export
      if (4 < srcOrientation && srcOrientation < 9) {
        canvas.width = height;
        canvas.height = width;
      } else {
        canvas.width = width;
        canvas.height = height;
      }

      // transform context before drawing image
      switch (srcOrientation) {
        case 2: ctx.transform(-1, 0, 0, 1, width, 0); break;
        case 3: ctx.transform(-1, 0, 0, -1, width, height); break;
        case 4: ctx.transform(1, 0, 0, -1, 0, height); break;
        case 5: ctx.transform(0, 1, 1, 0, 0, 0); break;
        case 6: ctx.transform(0, 1, -1, 0, height, 0); break;
        case 7: ctx.transform(0, -1, -1, 0, height, width); break;
        case 8: ctx.transform(0, -1, 1, 0, 0, width); break;
        default: break;
      }

      // draw image
      ctx.drawImage(img, 0, 0);

      // export base64
      callback(canvas.toDataURL());
    });

    img.src = srcBase64;
  }

  restoreDefault() {
    this.company.name = null;
    this.company.street1 = null;
    this.company.street2 = null;
    this.company.city = null;
    this.company.state = null;
    this.company.zip = null;
    this.company.telephone = null;
    this.company.fax = null;
    this.company.internet = null;
    this.company.logo1 = null;
    this.localStorage.setItem('company', this.company).subscribe(() => {
      console.log("restored to default");
    });
  }

}
