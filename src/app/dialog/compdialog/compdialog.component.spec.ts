import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompdialogComponent } from './compdialog.component';

describe('CompdialogComponent', () => {
  let component: CompdialogComponent;
  let fixture: ComponentFixture<CompdialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompdialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
