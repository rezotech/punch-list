import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsoledialogComponent } from './consoledialog.component';

describe('ConsoledialogComponent', () => {
  let component: ConsoledialogComponent;
  let fixture: ComponentFixture<ConsoledialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsoledialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsoledialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
