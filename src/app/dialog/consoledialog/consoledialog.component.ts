import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { ScService } from 'src/app/services/sc.service';

@Component({
  selector: 'app-consoledialog',
  templateUrl: './consoledialog.component.html',
  styleUrls: ['./consoledialog.component.css']
})
export class ConsoledialogComponent implements OnInit {
  logtxt: string[] = [];

  constructor(
    public dialogRef: MatDialogRef<ConsoledialogComponent>,
    private scService: ScService,
  ) { }

  close() {
    this.dialogRef.close();
  }

  clear() {
    this.logtxt.length = 0;
  }

  addLog(logValue: string) {
    this.logtxt.push(logValue);
  }

  ngOnInit() {
    this.scService.setConsoleDialog(this);
  }

}
