import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ScService } from 'src/app/services/sc.service';
import { Project } from 'src/models/project.model';

@Component({
  selector: 'app-projdelete',
  templateUrl: './projdelete.component.html',
  styleUrls: ['./projdelete.component.css']
})

export class ProjdeleteComponent implements OnInit {
  project: Project = new Project();
  project_number: string = "Project Number";
  project_name: string = "Project Name";

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ProjdeleteComponent>,
    protected localStorage: LocalStorage,
    private scService: ScService,
  ) {
    console.log("projkey = " + this.data.projkey);
    this.localStorage.getItem<Project>(this.data.projkey).subscribe(<Project>(proj) => {
      this.project = proj;
      this.project_number = "Project #: " + proj.number;
      this.project_name = "Project Name: " + proj.name;
    }, (error) => {
      console.log("currentProject ERROR!");
    });

  }

  ngOnInit() {
  }

  delete() {

    // delete all project photos. 
    if(this.project.issues!=null && this.project.issues.length>0) {
      for(var i=0; i<this.project.issues.length; i++) {
        var issue = this.project.issues[i];
        if(issue.photokeys!=null && issue.photokeys.length>0) {
          for(var j=0; j<issue.photokeys.length; j++) {
            this.localStorage.removeItem('photo-'+issue.photokeys[j]).subscribe(() => {
              console.log("photo deleted.");
            }, (error) => {
              console.error("error deleting photo.");
            });
          }
        }
      }
    }

    // delete projkey from project list. 
    var projectList = this.scService.getProjectList();
    const index = projectList.projkeys.indexOf(this.project.key, 0);
    if(index > -1) {
      projectList.projkeys.splice(index, 1);
    }
    this.localStorage.setItem('plist', projectList).subscribe(() => {
      console.log("project list updated!");
    }, (error) => {
      console.error("error updating project list.");
    });

    // delete project.
    this.localStorage.removeItem(this.project.key).subscribe(() => {
      console.log("project deleted.");
    }, (error) => {
      console.error("error deleting project.");
    });
    this.dialogRef.close();
  }
}
