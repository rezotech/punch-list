import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjdeleteComponent } from './projdelete.component';

describe('ProjdeleteComponent', () => {
  let component: ProjdeleteComponent;
  let fixture: ComponentFixture<ProjdeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjdeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjdeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
