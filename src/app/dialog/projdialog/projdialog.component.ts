import { Component, OnInit, Input, Inject, ElementRef, ViewChild } from '@angular/core';
import { Project } from 'src/models/project.model';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSelect } from '@angular/material';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ScService } from 'src/app/services/sc.service';
import { ProjdeleteComponent } from './projdelete/projdelete.component';

@Component({
  selector: 'app-projdialog',
  templateUrl: './projdialog.component.html',
  styleUrls: ['./projdialog.component.css']
})
export class ProjdialogComponent implements OnInit {
  @ViewChild(MatSelect, { read: ElementRef, static: true }) qualsel: MatSelect;
  project: Project = new Project();
  imgquality: string = "better";
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ProjdialogComponent>,
    protected localStorage: LocalStorage,
    private scService: ScService,
  ) {
    console.log("projkey = " + this.data.project.key);
    this.project = this.data.project;
   }

  ngOnInit() {
    console.log("img_qual = " + this.project.img_qual);
    if(this.project.img_qual == null) {
      this.imgquality = "good";
    } if(this.project.img_qual == this.scService.lowimgQual) {
      this.imgquality = "good";
    } if(this.project.img_qual == this.scService.midimgQual) {
      this.imgquality = "better";
    } if(this.project.img_qual == this.scService.higimgQual) {
      this.imgquality = "best"
    }
  }

  pnameBlur(event: any) {
    this.project.name = event.target.value;
    this.localStorage.setItem(this.project.key, this.project).subscribe(() => {
      console.log("project name updated!");
      this.scService.updateCurrProj();
    });
  }

  pnumberBlur(event: any) {
    this.project.number = event.target.value;
    this.localStorage.setItem(this.project.key, this.project).subscribe(() => {
      console.log("project number updated!");
    });
  }

  headerrighttextBlur(event: any) {
    this.project.top_right_text = event.target.value;
    this.localStorage.setItem(this.project.key, this.project).subscribe(() => {
      console.log("header right text updated!");
    });
  }

  footerlefttext1Blur(event: any) {
    this.project.bot_left_text_1 = event.target.value;
    this.localStorage.setItem(this.project.key, this.project).subscribe(() => {
      console.log("footer left text 1 updated!");
    });
  }

  footerlefttext2Blur(event: any) {
    this.project.bot_left_text_2 = event.target.value;
    this.localStorage.setItem(this.project.key, this.project).subscribe(() => {
      console.log("footer left text 2 updated!");
    });
  }

  reportdateBlur(event: any) {
    this.project.report_date = event.target.value;
    this.localStorage.setItem(this.project.key, this.project).subscribe(() => {
      console.log("report date updated!");
    });
  }

  onChange(event: any) {
    console.log("value = " + event.value);
    if(event.value==="good") {
      this.scService.getCurrProj().img_qual=this.scService.lowimgQual;
    }
    if(event.value==="better") {
      this.scService.getCurrProj().img_qual=this.scService.midimgQual;
    }
    if(event.value==="best") {
      this.scService.getCurrProj().img_qual=this.scService.higimgQual;
    }
    this.localStorage.setItem(this.project.key, this.project).subscribe(() => {
      console.log("image quality updated! = " + this.project.img_qual);
    });
  }

  restoreDefault() {
    this.project.name = null;
    this.project.number = null;
    this.project.top_right_text = null;
    this.project.bot_left_text_1 = null;
    this.project.bot_left_text_2 = null;
    this.project.report_date = null;
    this.localStorage.setItem(this.project.key, this.project).subscribe(() => {
      console.log("restored to default");
    });
  }

  delete() {
    const dialogRef = this.dialog.open(ProjdeleteComponent, {
      data: { projkey: this.project.key },
      panelClass: 'app-full-bleed-dialog',
      width: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog closed!");
      this.scService.getAppComp().update();
    });
    this.dialogRef.close();
  }

  

}
