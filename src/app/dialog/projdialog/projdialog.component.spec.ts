import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjdialogComponent } from './projdialog.component';

describe('ProjdialogComponent', () => {
  let component: ProjdialogComponent;
  let fixture: ComponentFixture<ProjdialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjdialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
