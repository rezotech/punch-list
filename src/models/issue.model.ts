import { Photo } from "./photo.model";

export class Issue {
    id: string;  
    description: string;
    name: string;
    trade: string;
    floor: string;
    location: string;
    no: number;
    photokeys: string[] = [];
    open_date: number;
    
    constructor() {
    }

}