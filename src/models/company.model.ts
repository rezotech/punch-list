import { Issue } from "./issue.model";

export class Company {
    key: string;
    id: number;
    name: string;
    number: string;
    street1: string;
    street2: string;
    city: string;
    state: string;
    zip: string;
    telephone: string;
    fax: string;
    internet: string;
    logo1: string;
    logo2: string;
    logo3: string;
}
