import { Issue } from "./issue.model";

export class Project {
    key: string;
    accountKey: string;
    id: number;
    name: string;
    number: string;
    issues: Issue[] = [];
    drawingkeys: string[] = [];
    img_qual: number = .3;
    top_right_text: string;
    bot_left_text_1: string;
    bot_left_text_2: string;
    report_date: string;
    
}
