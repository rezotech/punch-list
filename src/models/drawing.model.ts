import { Dwgmark } from './dwgmark.model';

export class Drawing {
    id = '';
    dataUrl = '';
    description = '';
    name = '';
    trade = '';
    sheet_no = '';
    filing_no = '';
    no = '';
    width = 100;
    height = 100;
    scale = 1;
    open_date = 0;
    dwgmarks: Dwgmark[] = [];

    constructor() {
    }

}
