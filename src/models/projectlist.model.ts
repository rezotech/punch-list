import { Project } from "./project.model";

export class ProjectList {
    id: number;
    projkeys: string[] = [];
    projnos: number = 0;
    aboutdiag: boolean = false;
}
